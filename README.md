READ.ME


1 - Deposer les 2 répertoire dans votre projet.
2 - Initialiser votre serveur APACHE TOMCAT ( version 8 minimum)
3 - Initialiser votre serveur de BDD
4 - Modifier le fichier application.properties  dans \complete\src\main\resources
        spring.datasource.url=jdbc:mysql://${MYSQL_HOST:localhost}:3306/spring
        spring.datasource.username=root
        spring.datasource.password=root
	- username / mot de passe du serveur de bdd
	- nom de la BDD
5 - Initialiser le projet dans /complete, en ligne de commande :
	mvnw spring-boot:run
6 - Les tables sont automatiquement générées
	Initialiser les données de /prérequis
		- ephad.sql
		- role.sql
		- user.sql
		- user_roles.sql
		- villefrance.sql
7 - Connectez-vous via votre navigateur web
	localhost:8080
	Utilisateur: admin , mot de passe: adminadmin

8 - Modifier le compte mail pour la réception et distribution des mails dans la classe MailController

Vous avez désormais accès à la gestion des admin. Créez un admin pour chaque ephad de votre choix (par soucis de facilité , lors de la création du compte, l'id de chaque ephad est indiqué à côté de sa raison sociale ).
Vous pouvez également créer sur la page d'accueil un compte visiteur (famille ou association). La génération de la page peut être longue. En effet celle-ci charge les 36 000 communes. Uns script JS peut accélérer l'affichage (afficage des villes par CP  par exemple). Ce n'était pas le sujet du projet qui était axé sur JAVA et n'a donc pas été implémenté.
Le visiteur peut soumettre toute demande de visite à tout ephad renseigné dans le projet.
Lorsque l'admin le lie à un résidant il peut consulter selon ses droits (famille-association), ses visites, leur statut et le calendrier du résidant.
L'admin de l'ephad précédemment créé peut ajouter des médecins et gérer son Ephad (visiteurs, résidants,...). 