package com.gendteam.project.Controller;

import com.gendteam.project.model.User;
import com.gendteam.project.model.VilleFrance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.lang.reflect.Field;
import java.security.Principal;

@Controller
public class AdminsController {
    
    private final com.gendteam.project.repository.UserRepository UserRepository;
    private VilleFrance myRequest;

    @Autowired
    public AdminsController( com.gendteam.project.repository.UserRepository UserRepository) {
        this.UserRepository = UserRepository ;
    }

    /**
     *
     * @param pageable
     * @param request
     * @param model
     * @param principal
     * @return
     */
    // Affichage de la liste complète
    @RequestMapping("admins/index")
    @GetMapping("admins/index")
    public String adminsIndex(@PageableDefault(size = 6) Pageable pageable, HttpServletRequest request,
                              Model model, Principal principal) {
        // Affichage de données dans la console
        SecurityContext ctx = SecurityContextHolder.getContext();
        //Object currentUser = ctx.getAuthentication().getPrincipal();


        User myUser = UserRepository.findByUsername(principal.getName());
        model.addAttribute("username", myUser);


//        Page<User> page = UserRepository.findAllByRolesIs((long) 1);
//
//                Pager(model, page);
//
//        model.addAttribute("page", page);
        return "admins/index";
    }

    /**
     *
     * @param id
     * @param model
     * @return
     */
    // Affichage d'un objet
    @GetMapping("admins/show/{id}")
    public String showModel(@PathVariable("id") long id, Model model) {
        User user = UserRepository.findById(id);
        model.addAttribute("usere", user);
        return "admins/show";
    }

    /**
     *
     * @param id
     * @param model
     * @return
     */
    @GetMapping("admins/edit/{id}")
    public String editModel(@PathVariable("id") long id, Model model) {
        User user = UserRepository.findById(id);
        model.addAttribute("user", user);
        return "admins/edit";
    }

    /**
     *
     * @param id
     * @param user
     * @param result
     * @param model
     * @return
     */
    @PostMapping("admins/update/{id}")
    public String updateUser(@PathVariable("id") long id, @Valid User user, BindingResult result, Model model) {
        if (result.hasErrors()) {
            user.setId(id);
            return "/admins/edit";
        }
        User userSource = UserRepository.findById(id);
        try {
            User userSave = (User) copyFields(userSource, user);
            UserRepository.save(userSave);
            return "redirect:/admins/show/" + user.getId();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "/admins/edit";
    }



    /**
     *
     * @param source
     * @param destination
     * @return
     * @throws Exception
     */
    private Object copyFields(Object source, Object destination) throws Exception{
        Class sourceClass = source.getClass();
        Field[] fields = sourceClass.getDeclaredFields();
        Class destinationClass = destination.getClass();
        Field[] duplicateFields = destinationClass.getDeclaredFields();
        //if source class and destination class are same do
        for (Field field : fields) {
            field.setAccessible(true);
            for ( Field duplicateField : duplicateFields) {
                duplicateField.setAccessible(true);
                //check value
                Object value = field.get(source);
                Object destinationValue = duplicateField.get(destination);
                //Compare values and set
                //duplicateField.set(destination,field.get(source));
            }
        }
        return destination;
    }

    /**
     *
     * @param model
     * @param page
     */
    private void Pager(Model model, Page page){
        int totalPages = page.getTotalPages();
        int begin = 0;
        int end = 0;
        int pageCourante = page.getNumber();

        if(totalPages ==0 ){
            begin = 0;
            end = 0;
        }
        else if(totalPages <6){
            begin = 0;
            end = totalPages-1;
        }
        else if(pageCourante +6 >= totalPages){
            begin = totalPages-7;
            end = totalPages-1;
        }
        else if(pageCourante<4){
            begin = 0 ;
            end = 7;
        }else{
            begin= pageCourante-3;
            end=pageCourante+3;
        }

        model.addAttribute("pagerBegin", begin );
        model.addAttribute("pagerEnd", end);
        model.addAttribute("totalPages", totalPages);
    }

}