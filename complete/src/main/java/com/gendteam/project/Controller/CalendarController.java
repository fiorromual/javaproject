package com.gendteam.project.Controller;

import com.gendteam.project.model.Event;
import com.gendteam.project.model.Resource;
import com.gendteam.project.model.Visited;
import com.gendteam.project.repository.EventRepository;
import com.gendteam.project.repository.ResourceRepository;
import com.gendteam.project.repository.VisitedRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.time.LocalDateTime;

/**
 *
 */
@RestController
public class CalendarController {


    private final com.gendteam.project.repository.VisitedRepository VisitedRepository;

    public  CalendarController (VisitedRepository VisitedRepository){
        this.VisitedRepository = VisitedRepository ;

    }

    @Autowired
    EventRepository er;

    @Autowired
    ResourceRepository rr;

    @RequestMapping("/api")
    @ResponseBody
    String home() {
        return "Welcome!";
    }

    @RequestMapping("/api/resources")
    Iterable<Resource> resources() {
        return rr.findAll();
    }

    /**
     *
     * @param id  visitedId
     * @param start  Begin visit
     * @param end   End visit
     * @return Return all events
     */
    @GetMapping("api/events/{id}")
    Iterable<Event> events(@PathVariable("id") long id,
                           @RequestParam("start") @DateTimeFormat(iso = ISO.DATE_TIME) LocalDateTime start,
                           @RequestParam("end") @DateTimeFormat(iso = ISO.DATE_TIME) LocalDateTime end){
        System.out.println("****************************************La variable passee est :"+ id);
        //Gestion des droits
        System.out.println("*************************** debut de la demande : " + start) ;
        System.out.println("************************** fin de la demande : " + end)  ;
        Visited visitedId = VisitedRepository.findById(id);
        System.out.println(start);
        System.out.println(end);

        return er.findBetween(start, end, visitedId);
    }

    /**
     *
     * @param params  resources params
     * @return  return created event
     */
    @PostMapping("api/events/create")
    @Transactional
    Event createEvent(@RequestBody EventCreateParams params) {

        Resource r = null;

        if (params.resource != null) {
            r = rr.findOne(params.resource);
        }

        Event e = new Event();
        e.setStart(params.start);
        e.setEnd(params.end);
        e.setText(params.text);
        e.setResource(r);

        er.save(e);

        return e;
    }

    /**
     *
     params  resources params
     * @return  return modified event
     */
    @PostMapping("api/events/move")
    @Transactional
    Event moveEvent(@RequestBody EventMoveParams params) {

        Event e = er.findOne(params.id);

        Resource r = null;

        if (params.resource != null) {
            r = rr.findOne(params.resource);
        }

        e.setStart(params.start);
        e.setEnd(params.end);
        e.setResource(r);

        er.save(e);

        return e;
    }

    /**
     *
     * @param params of event
     * @return  modify color of event
     */
    @PostMapping("api/events/setColor")
    @Transactional
    Event setColor(@RequestBody SetColorParams params) {

        Event e = er.findOne(params.id);
        e.setColor(params.color);
        er.save(e);

        return e;
    }

    /**
     *
     */
    public static class EventCreateParams {
        public LocalDateTime start;
        public LocalDateTime end;
        public String text;
        public Long resource;
    }

    /**
     *
     */
    public static class EventMoveParams {
        public Long id;
        public LocalDateTime start;
        public LocalDateTime end;
        public Long resource;
    }

    public static class SetColorParams {
        public Long id;
        public String color;
    }


}