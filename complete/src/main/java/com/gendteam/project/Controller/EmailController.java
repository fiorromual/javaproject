package com.gendteam.project.Controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.*;
import javax.mail.internet.*;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;

@RestController
public class EmailController {
    @RequestMapping(value = "/sendemail")
    public String sendEmail() {
        return "Email envoyé avec succès";
    }


    public static void sendmail(
            String userMail,
            String Subject,
            String content )
            throws MessagingException, IOException {
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {

                return new PasswordAuthentication("gendteam.java@gmail.com", "RFLgendteam2020");
            }
        });
        //session.getProperties().put("mail.smtp.ssl.trust", "smtp.gmail.com");
        Message msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress("gendteam.java@gmail.com", false));
        msg.setText("Information");
        msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse( userMail ));
        msg.setSubject(Subject);
        msg.setContent( content, "text/html");
        msg.setSentDate(new Date());

        MimeBodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent(content, "text/html");

        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);
        // Si on voulait envoyer des pièces jointes
//        MimeBodyPart attachPart = new MimeBodyPart();

//        attachPart.attachFile();
//        multipart.addBodyPart(attachPart);
        msg.setContent(multipart);
        Transport.send(msg);
    }
}