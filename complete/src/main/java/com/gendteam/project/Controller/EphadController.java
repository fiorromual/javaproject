package com.gendteam.project.Controller;

import com.gendteam.project.model.Ephad;
import com.gendteam.project.model.User;
import com.gendteam.project.repository.EphadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@Controller
public class EphadController {

    private final com.gendteam.project.repository.EphadRepository EphadRepository;

    @Autowired
    private com.gendteam.project.repository.UserRepository UserRepository;

    @Autowired
    public EphadController( EphadRepository EphadRepository) {
        this.EphadRepository = EphadRepository;
    }

    /**
     *
     * @param pageable  pageable object
     * @param request  of user
     * @param ephadForm  search form
     * @param model  to the template
     * @return  index template
     */
    // Affichage de la liste complète
    @RequestMapping("ephad/index")
    @GetMapping("ephad/index")
    public String indexModel(@PageableDefault(size = 6) Pageable pageable, HttpServletRequest request,
                                   Ephad ephadForm, Model model) {
        // Affichage de données dans la console

        Page<Ephad> page = (Page<Ephad>) filter(pageable, model,request, ephadForm);

        Pager(model, page);

        model.addAttribute("page", page);
        model.addAttribute("ephadForm", ephadForm);
        return "ephad/index";
    }

    /**
     *
     * @param model for template
     * @param principal to get session
     * @return medIndex
     */
    // Affichage de la liste complète
    @RequestMapping("ephad/medIndex")
    @GetMapping("ephad/medIndex")
    public String medIndexModel( Model model, Principal principal) {
        SecurityContext ctx = SecurityContextHolder.getContext();
        User myUser = UserRepository.findByUsername(principal.getName());

        List<User> medUsers = UserRepository.findAllByEphadIdAndRolesId_Id(myUser.getEphadId(), 4);

        model.addAttribute("medUsers", medUsers);
        return "users/medIndex";
    }

    /**
     *
     * @param id of the object
     * @param model  to the template
     * @return  template for object view
     */
    // Affichage d'un objet
    @GetMapping("ephad/show/{id}")
    public String showModel(@PathVariable("id") long id, Model model) {
        Ephad ephad = (Ephad) EphadRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Ephad inconnu:" + id));
        model.addAttribute("ephad", ephad);
        return "ephad/show";
    }

    @GetMapping("ephad/showEphadCapacity")
    public String showEphadCapacity( Model model, Principal principal) {
        SecurityContext ctx = SecurityContextHolder.getContext();
        User myUser = UserRepository.findByUsername(principal.getName());
        model.addAttribute("ephad", myUser.getEphadId());
        return "ephad/showEphadCapacity";
    }

    @GetMapping("ephad/editEphadCapacity")
    public String editEphadCapacity( Model model, Principal principal) {
        SecurityContext ctx = SecurityContextHolder.getContext();
        User myUser = UserRepository.findByUsername(principal.getName());
        model.addAttribute("ephad", myUser.getEphadId());
        return "ephad/editEphadCapacity";
    }


    @PostMapping("ephad/editEphadCapacity/{id}")
    public String editEphadCapacity(@PathVariable("id") long id, @Valid Ephad ephad, BindingResult result, Model model) {
        if (result.hasErrors()) {
            ephad.setId(id);
            return "/ephad/editEphadCapacity";
        }
        Ephad ephadSource = (Ephad) EphadRepository.findById(id).orElseThrow(()-> new IllegalArgumentException("Ephad inconnu:" + id));
        try {
            ephadSource.setCapaciteVisite(ephad.getCapaciteVisite());
            EphadRepository.save(ephadSource);
            return "redirect:/ephad/showEphadCapacity/";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "ephad/showEphadCapacity";
    }


    /**
     *
     * @param pageable List of pageable object
     * @param model  Model to the template
     * @param request  of user
     * @param EphadForm  search form
     * @return list of object in a pager
     */
    private Object filter(@PageableDefault(size = 6) Pageable pageable, Model model, HttpServletRequest request,
                          Ephad EphadForm){

        Ephad EphadFilter = (Ephad) getSessionStatus(request, EphadForm);

        if( (EphadFilter.getVille()== null && EphadFilter.getCodePostal()== null)
                || EphadFilter == null) {
            Page<Ephad> page = EphadRepository.findAll(pageable);

            return page;
        }
        else{
            String ville = new String();
            String codePostal = new String();
            ville = EphadFilter.getVille();
            codePostal = EphadFilter.getCodePostal();

            Page<Ephad> page = EphadRepository.findAllByVilleOrCodePostal(ville, codePostal, pageable);

            return page;
        }
    }

    /**
     *
     * @param request of the user
     * @param ephadForm  search form
     * @return  catch the actual request
     */
    private Object getSessionStatus(HttpServletRequest request, Ephad ephadForm){
        HttpSession session = request.getSession();
        // Des données sont en session
        Object myRequest = ephadForm;
       Ephad myRequestReset = new Ephad("", "");

        if (session.getAttribute("selectRequest") != null ) {
            myRequest = session.getAttribute("selectRequest");
//            session.setAttribute("selectRequestPersist","En mémoire");
        }
        // Des données nouvelles sont mises en session
        if( request.getParameter("filter_action") != null &&
                request.getParameter("filter_action").equals("filter")){
            session.setAttribute("selectRequest",ephadForm);
            myRequest = session.getAttribute("selectRequest");
//            session.setAttribute("selectRequestPersist","Ajouté en mémoire");
        }
        // Reset est activé, les données de la session sont vidées
        if( request.getParameter("filter_action") != null &&
                request.getParameter("filter_action").equals("reset")){
//            model.addAttribute("search2", "reset validé");
            myRequest = myRequestReset;
            session.setAttribute("selectRequest",myRequest);
        }
        return myRequest;
    }

    /**
     *
     * @param model  information for template
     * @param page  List of object for template
     */
    private void Pager(Model model, Page page){
        int totalPages = page.getTotalPages();
        int begin = 0;
        int end = 0;
        int pageCourante = page.getNumber();

        if(totalPages ==0 ){
            begin = 0;
            end = 0;
        }
        else if(totalPages <6){
            begin = 0;
            end = totalPages-1;
        }
        else if(pageCourante +6 >= totalPages){
            begin = totalPages-7;
            end = totalPages-1;
        }
        else if(pageCourante<4){
            begin = 0 ;
            end = 7;
        }else{
            begin= pageCourante-3;
            end=pageCourante+3;
        }

        model.addAttribute("pagerBegin", begin );
        model.addAttribute("pagerEnd", end);
        model.addAttribute("totalPages", totalPages);
    }

    /**
     *
     * @param request by user
     * @param EphadForm ephad form for research
     * @return
     */
    private Ephad sessionRequest(HttpServletRequest request, Ephad EphadForm){
        HttpSession session = request.getSession();
        if( request.getAttribute("selectRequest") == null && EphadForm == null )
            return null ;
        else if(EphadForm != null ) {
            return (Ephad) request.getAttribute("selectRequest");
        }
        else{
            return (Ephad) request.getAttribute("selectRequest");
        }
    }

}