package com.gendteam.project.Controller;

import com.gendteam.project.form.EventForm;
import com.gendteam.project.form.EventFormFilter;
import com.gendteam.project.form.EventNewsForm;
import com.gendteam.project.model.Event;
import com.gendteam.project.model.User;
import com.gendteam.project.model.Visited;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.security.Principal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.*;

@Controller
public class EventController {



    private final com.gendteam.project.repository.EventRepository EventRepository;
    private final com.gendteam.project.validator.EventValidator EventValidator;

    @Autowired
    private com.gendteam.project.repository.VisitedRepository VisitedRepository;



    @Autowired
    private com.gendteam.project.repository.UserRepository UserRepository;


    @Autowired
    public EventController( com.gendteam.project.repository.EventRepository EventRepository,
                            com.gendteam.project.validator.EventValidator EventValidator) {
        this.EventRepository = EventRepository;
        this.EventValidator = EventValidator;
    }

    /**
     *
     * @param pageable  pageable object
     * @param request  of user
     * @param eventFormFilter  search form
     * @param model  to the template
     * @return  index template
     */
    // Affichage de la liste complète
    @RequestMapping("event/index")
    @GetMapping("event/index")
    public String indexEventModel(@PageableDefault(size = 6) Pageable pageable, HttpServletRequest request,
                                  EventFormFilter eventFormFilter, Model model, Principal principal) throws ParseException {

        Page<Event> page = (Page<Event>) filter(pageable, model,request, eventFormFilter, principal);

        Pager(model, page);

        model.addAttribute("page", page);
        model.addAttribute("eventForm", eventFormFilter);
        return "event/index";
    }

    /**
     *
     * @param pageable  pageable object
     * @param model  to the template
     * @return  index template
     */
    // Affichage de la liste complète
    @RequestMapping("event/oldindex")
    @GetMapping("event/oldindex")
    public String indexOldEventModel(@PageableDefault(size = 6) Pageable pageable,
                                  Model model, Principal principal) throws ParseException {
        // Affichage de données dans la console
        SecurityContext ctx = SecurityContextHolder.getContext();
        User myUser = UserRepository.findByUsername(principal.getName());
        LocalDateTime tomorrow = getDayAfter();
        Page<Event> page = (Page<Event>) EventRepository.findAllByStartBeforeAndAndUserId(tomorrow
                , myUser.getId() , pageable );

        // Date du jour
//        LocalDateTime.ofInstant(Calendar.getInstance().toInstant(),ZoneId.systemDefault())
        Pager(model, page);
        model.addAttribute("page", page);
        return "event/oldindex";
    }

    /**
     *
     * @param id of the object
     * @param model  to the template
     * @return  template for object view
     */
    // Affichage d'un objet
    @GetMapping("event/show/{id}")
    public String showModel(@PathVariable("id") long id, Model model) {
        Event event = (Event) EventRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Event inconnu:" + id));
        model.addAttribute("event", event);
        return "event/show";
    }


    /**
     *
     * @param id of the object
     * @param model  to the template
     * @return  template for object view
     */
    // Affichage d'un objet
    @Transactional
    @GetMapping("event/delete/{id}")
    public String deleteModel(@PathVariable("id") long id, Model model) {
        Event event = (Event) EventRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Event inconnu:" + id));
        EventRepository.deleteById(event.getId());
        return "/welcome";
    }

    /**
     *
     * @param id
     * @param model
     * @return
     */
    @GetMapping("event/edit/{id}")
    public String editModel(@PathVariable("id") long id, Model model) {
        Event event = (Event) EventRepository.findById(id).orElseThrow(() ->  new IllegalArgumentException("Visite inconnue:" + id));
        EventForm myForm = new EventForm();
        User userId = UserRepository.findById( event.getUserId());

        model.addAttribute("userId", userId);
        model.addAttribute("myForm", myForm);
        model.addAttribute("event", event);
        return "event/edit";
    }

    /**
     *
     * @param id eventIf
     * @param myForm for Event modification
     * @param result
     * @param model for teemplate injection
     * @return go back to event Index
     */
    @Transactional
    @PostMapping("event/update/{id}")
    public String updateUser(@PathVariable("id") long id, @Valid EventForm myForm, BindingResult result, Model model) {
        Event event = (Event) EventRepository.findById(id).orElseThrow(() ->  new IllegalArgumentException("Visite inconnue:" + id));
        if (result.hasErrors()) {
            event.setId(id);
            return "/event/edit";
        }

        // Récupération de l'utilisateur
        User user = UserRepository.findById( event.getUserId());

        // Contrôle des données et implémentation dans color
        String addString ;
        switch (myForm.getComment()) {
            case "#cc0001":  addString = " Demande Acceptée par le gestionnaire ";
                event.setColor("#cc0000");
                event.setText("N°" + event.getId() +addString);
                break;
            case "#cc0002":  addString = " Demande accepté sous réserve du médecin";
                event.setColor("#cc0000");
                event.setText("N°" + event.getId() +addString);
                break;
            case "#cc0000":  addString= " Demande Accepté par le médecin";
                event.setColor("#cc0000");
                event.setText("N°" + event.getId() +addString);
                break;
            case "#f1c232":  addString= " Demande refusée par le médecin";
                event.setColor("#f1c232");
                event.setText("N°" + event.getId() +addString);
                break;
            default: ;
                break;
        }

        try {
            EventRepository.save(event);
            if(event.getColor()=="#cc0000") {
                EmailController.sendmail(
                        user.getEmail(),
                        "Demande de visite M ou Mme" + event.getVisitedId().getFirstName(),
                        event.getEphadId().getRaisonSociale() + " " +
                                "Bonjour, votre demande de visite a été visée. Veuillez vérifier son état." +
                                ""
                );
            }
            else{
                EmailController.sendmail(
                        user.getEmail(),
                        "Demande de visite M ou Mme" + event.getVisitedId().getFirstName(),
                        event.getEphadId().getRaisonSociale() + " " +
                                "Bonjour, votre demande de visite a été visée. " +
                                "Malheureusement nous ne pouvons donner suite à votre demande." +
                                "Nous vous prions de bien vouloir renouveler votre demande ou nous contacter." +
                                ""
                );
                // Suppression de la demande selon les prérequis
                System.out.println("**************** Suppression de l'instance :" + event.getId());
                EventRepository.deleteById(event.getId());
            }
            return "redirect:/event/index";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "/event/edit";
    }

    /**
     *
     * @param id of the object
     * @param model  to the template
     * @return  template for object view
     */
    // Affichage d'un objet
    @GetMapping("event/qrCode/{id}")
    public String qrCodeModel(@PathVariable("id") long id, Model model) {
        Event event = (Event) EventRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Event inconnu:" + id));
        // Génération de la passPhrase
        // Récupération de l'utilisateur
        User user = UserRepository.findById( event.getUserId());
        String passPhrase = event.getEphadId().getRaisonSociale()+ " " + user.getLastName() +" "+ user.getFirstName()+" "+
                event.getVisitedId().getLastName() +" "+ event.getVisitedId().getFirstName()+" "+
                event.getStart();
        try {
            BufferedImage myQrCode2 = getQRCode(passPhrase, 250,250);

            ByteArrayOutputStream output = new ByteArrayOutputStream();
            ImageIO.write(myQrCode2, "png", output);
            String imageAsBase64 = Base64.getEncoder().encodeToString(output.toByteArray());
            System.out.println("**************************buff 3"+imageAsBase64);
            model.addAttribute("qrCode3", imageAsBase64);
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("user", user);
        model.addAttribute("event", event);
        return "event/qrcode";
    }


    /**
     *
     * @param model to template
     * @return template news
     */
    @GetMapping("event/news")
    public String newsEventModel(Model model) {
        EventNewsForm eventNewsForm = new EventNewsForm();
        model.addAttribute("eventNewsForm",  eventNewsForm);
        return "event/news";
    }


    @PostMapping("event/newsList")
    public String newsListModel(@Valid EventNewsForm eventNewsForm, BindingResult result, Model model, Principal principal) {
        EventValidator.validateDate(eventNewsForm, result);
        if (result.hasErrors()) {
            return "event/news";
        }

        System.out.println("************************** newsListModel");
        Date myDate = eventNewsForm.getDateNews();
        System.out.println("**************************date demandée :"+myDate);
        // Récupération des crédentials pour récupérer l'Ephad correspondant
        User myUser = UserRepository.findByUsername(principal.getName());

        LocalDateTime ldt = LocalDateTime.ofInstant(myDate.toInstant(), ZoneId.systemDefault());
        //Date du jour suivant
        LocalDateTime tomorrow = LocalDateTime.from(ldt.plusDays(1));

        List<Event> newsEvent = EventRepository.findAllByEphadIdAndStartAfterAndStartBeforeAndColor(myUser.getEphadId(),ldt, tomorrow, "#cc0000");

        model.addAttribute("newsEvents",  newsEvent);
        return "/event/newsList";
    }


    /**
     *
     * @param targetUrl PassPhrase
     * @param width largeur
     * @param height longueur
     * @return
     */
    public static BufferedImage getQRCode(String targetUrl, int width,
                                          int height) {
        try {
            Hashtable<EncodeHintType, Object> hintMap = new Hashtable<>();
            hintMap.put(EncodeHintType.ERROR_CORRECTION,
                    ErrorCorrectionLevel.L);
            QRCodeWriter qrCodeWriter = new QRCodeWriter();
            BitMatrix byteMatrix = qrCodeWriter.encode(targetUrl,
                    BarcodeFormat.QR_CODE, width, height, hintMap);
            int CrunchifyWidth = byteMatrix.getWidth();
            BufferedImage image = new BufferedImage(CrunchifyWidth,
                    CrunchifyWidth, BufferedImage.TYPE_INT_RGB);
            image.createGraphics();
            Graphics2D graphics = (Graphics2D) image.getGraphics();
            graphics.setColor(Color.WHITE);
            graphics.fillRect(0, 0, CrunchifyWidth, CrunchifyWidth);
            graphics.setColor(Color.BLACK);
            for (int i = 0; i < CrunchifyWidth; i++) {
                for (int j = 0; j < CrunchifyWidth; j++) {
                    if (byteMatrix.get(i, j)) {
                        graphics.fillRect(i, j, 1, 1);
                    }
                }
            }
            return image;
        } catch (WriterException e) {
            e.printStackTrace();
            throw new RuntimeException("Erreur création QRCode");
        }
    }

    /**
     *
     * @param pageable List of pageable object
     * @param model  Model to the template
     * @param request  of user
     * @param EventFormFilter  search form
     * @return list of object in a pager
     */
    private Object filter(@PageableDefault(size = 6) Pageable pageable, Model model, HttpServletRequest request,
                          EventFormFilter EventFormFilter , Principal principal) throws ParseException {


        EventFormFilter eventFormFilter = (EventFormFilter) getSessionStatus(request, EventFormFilter);
        User user = UserRepository.findByUsername(principal.getName());
        Long myId = eventFormFilter.getId();
        if(eventFormFilter.getId()== null || eventFormFilter.getId() <1){
            myId = Long.valueOf(0);
        }
        System.out.println("***************** valeur de l'id recherché :" +eventFormFilter.getId());
        System.out.println("***************** valeur de myId :" +myId);
        System.out.println("***************** valeur de l'id recherché :" +eventFormFilter.getId());
        System.out.println("***************** valeur de la personne cherchée :" +eventFormFilter.getVisited());
        System.out.println("***************** valeur du filtre :" +eventFormFilter);

        if(
                (myId == 0 && eventFormFilter.getVisited() == null || eventFormFilter == null) &&
                        request.isUserInRole("ROLE_ADMIN") || request.isUserInRole("ROLE_MED")
        ){
            Page<Event> page = EventRepository.findAllByEphadId(user.getEphadId(),pageable);

            return page;
        }
        // Si c'est un simple user connecté
        else if((request.isUserInRole("ROLE_USER") || request.isUserInRole("ROLE_ASSO"))){
            LocalDateTime tomorrow = getDayAfter();
            Page<Event> page = EventRepository.findAllByUserIdAndStartAfter(user.getId(), tomorrow, pageable);

            return page;
        }
        else{
;
            String VisitedName = new String();
            Long idEvent = eventFormFilter.getId();
            String visitedName = eventFormFilter.getVisited();
            Visited visitedId = VisitedRepository.findByLastName(visitedName);

            Page<Event> page = EventRepository.findAllByIdOrVisitedId(idEvent, visitedId, pageable);

            return page;
        }
    }

    /**
     *
     * @param request of the user
     * @param eventForm  search form
     * @return  catch the actual request
     */
    private Object getSessionStatus(HttpServletRequest request, EventFormFilter eventForm){
        HttpSession session = request.getSession();
        // Des données sont en session
        Object myRequest = eventForm;
        EventFormFilter myRequestReset = new EventFormFilter(null, "");

        if (session.getAttribute("selectRequest") != null ) {
            myRequest = session.getAttribute("selectRequest");
        }
        // Des données nouvelles sont mises en session
        if( request.getParameter("filter_action") != null &&
                request.getParameter("filter_action").equals("filter")){
            session.setAttribute("selectRequest",eventForm);
            myRequest = session.getAttribute("selectRequest");
        }
        // Reset est activé, les données de la session sont vidées
        if( request.getParameter("filter_action") != null &&
                request.getParameter("filter_action").equals("reset")){
            myRequest = myRequestReset;
            session.setAttribute("selectRequest",myRequest);
        }
        return myRequest;
    }

    /**
     *
     * @param model  information for template
     * @param page  List of object for template
     */
    private void Pager(Model model, Page page){
        int totalPages = page.getTotalPages();
        int begin = 0;
        int end = 0;
        int pageCourante = page.getNumber();

        if(totalPages ==0 ){
            begin = 0;
            end = 0;
        }
        else if(totalPages <6){
            begin = 0;
            end = totalPages-1;
        }
        else if(pageCourante +6 >= totalPages){
            begin = totalPages-7;
            end = totalPages-1;
        }
        else if(pageCourante<4){
            begin = 0 ;
            end = 7;
        }else{
            begin= pageCourante-3;
            end=pageCourante+3;
        }

        model.addAttribute("pagerBegin", begin );
        model.addAttribute("pagerEnd", end);
        model.addAttribute("totalPages", totalPages);
    }

    /**
     *
     * @param request by user
     * @param EventForm event form for research
     * @return
     */
    private Event sessionRequest(HttpServletRequest request, Event EventForm){
        HttpSession session = request.getSession();
        if( request.getAttribute("selectRequest") == null && EventForm == null )
            return null ;
        else if(EventForm != null ) {
            return (Event) request.getAttribute("selectRequest");
        }
        else{
            return (Event) request.getAttribute("selectRequest");
        }
    }



    /**
     *
     * @param request of user
     * @param binder Modify date for Mysql
     */
    @InitBinder
    protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, null,  new CustomDateEditor(dateFormat, true));
    }

    private LocalDateTime getDayAfter() throws ParseException {
        //SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, +1);
        Date tomorrow = calendar.getTime();

        LocalDateTime result = LocalDateTime.ofInstant(tomorrow.toInstant(),ZoneId.systemDefault());

        return result;
    }

}