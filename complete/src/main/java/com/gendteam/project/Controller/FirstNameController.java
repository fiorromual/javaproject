package com.gendteam.project.Controller;

import com.gendteam.project.model.Ephad;
import com.gendteam.project.model.FirstName;
import com.gendteam.project.model.LastName;
import com.gendteam.project.model.Visited;
import com.gendteam.project.repository.EphadRepository;
import com.gendteam.project.repository.VisitedRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class FirstNameController {

    private final com.gendteam.project.repository.LastNameRepository LastNameRepository;
    private final com.gendteam.project.repository.FirstNameRepository FirstNameRepository;
    private final com.gendteam.project.repository.EphadRepository EphadRepository;
    private final com.gendteam.project.repository.VisitedRepository VisitedRepository;

    @Autowired
    public FirstNameController(com.gendteam.project.repository.LastNameRepository LastNameRepository,
                               com.gendteam.project.repository.FirstNameRepository FirstNameRepository,
                               EphadRepository EphadRepository,
                               VisitedRepository VisitedRepository


    ) {
        this.LastNameRepository = LastNameRepository;
        this.FirstNameRepository = FirstNameRepository;
        this.EphadRepository = EphadRepository;
        this.VisitedRepository = VisitedRepository;
        ;
    }

    /**
     *
     * @return go back to index visited
     */
    @GetMapping("visited/allnew")
    public String newVisited() {

        Long nbEphad = EphadRepository.findCount();
        Long hasardEphad = Long.valueOf((int)(Math.random() * nbEphad));
        Ephad ephad = new Ephad();
        System.out.println("*******************************nb ephad" + nbEphad);
        Integer nbVisited =0;

        Long nbFirstName = FirstNameRepository.findCount();
        Long hasardFirstName = Long.valueOf((int)(Math.random() * nbFirstName));
        FirstName firstName = FirstNameRepository.findOne(hasardFirstName-1);

        Long nbLastName = LastNameRepository.findCount();
        Long hasardLastName = Long.valueOf((int)(Math.random() * nbLastName));
        LastName lastName = LastNameRepository.findOne(hasardLastName-1);

        Visited visited = new Visited();
        for(long i=1;i<=nbEphad; i++) {
            System.out.println("*******************************nb Ehpad" + i);
            ephad = EphadRepository.findOne(i);
            nbVisited = (int) (0.75 * ephad.getCapaciteComplet());
            for(long j=1;j<=nbVisited; j++) {
                visited = new Visited();
                hasardFirstName = Long.valueOf((int)(Math.random() * nbFirstName));
                firstName = FirstNameRepository.findOne(hasardFirstName-1);

                hasardLastName = Long.valueOf((int)(Math.random() * nbLastName));
                lastName = LastNameRepository.findOne(hasardLastName-1);

                visited.setLastName(lastName.getLastName());
                visited.setFirstName(firstName.getFirstName());
                visited.setAlive(true);
                visited.setEphadId(ephad);

                //Sauvegarde
                VisitedRepository.save(visited);
                visited.toString();
            }
        }

        return "redirect:/visited/index";
    }
}
