package com.gendteam.project.Controller;

import com.gendteam.project.model.Ephad;
import com.gendteam.project.model.ObjectForm;
import com.gendteam.project.model.User;
import com.gendteam.project.model.VilleFrance;
import com.gendteam.project.repository.EphadRepository;
import com.gendteam.project.service.SecurityService;
import com.gendteam.project.service.UserService;
import com.gendteam.project.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.List;

@Controller
public class UserController {

    private final com.gendteam.project.service.UserService UserService;
    private final com.gendteam.project.validator.UserValidator UserValidator;
    private final com.gendteam.project.repository.UserRepository UserRepository;
    private final com.gendteam.project.repository.EphadRepository EphadRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private com.gendteam.project.repository.VilleFranceRepository VilleFranceRepository;

    @Autowired
    private UserValidator userValidator;

    @Autowired
    public UserController( UserService UserService , EphadRepository EphadRepository,
                           com.gendteam.project.repository.UserRepository UserRepository,
                           UserValidator UserValidator) {
        this.UserService = UserService;
        this.UserRepository = UserRepository ;
        this.UserValidator = UserValidator;
        this.EphadRepository = EphadRepository;

    }

    /**
     *
     * @param model to template
     * @return to registration template
     */
    @GetMapping("/registration")
    public String registration(Model model) {
        List<VilleFrance> villeFrance = VilleFranceRepository.findAll();
        ObjectForm myForm= new ObjectForm();

        model.addAttribute("myForm", myForm);
        model.addAttribute("villeFrance", villeFrance);
        model.addAttribute("userForm", new User());

        return "registration";
    }

    /**
     *
     * @param userForm  userForm by registration
     * @param bindingResult  Form control
     * @return  go back to welcome
     */
    @PostMapping("/registration")
    public String registration(@ModelAttribute("userForm") User userForm, BindingResult bindingResult) {
        UserValidator.validate(userForm, bindingResult);
        if (bindingResult.hasErrors()) {
            return "registration";
        }
        userService.save(userForm);
        securityService.autoLogin(userForm.getUsername(), userForm.getPasswordConfirm());

        return "redirect:/welcome";
    }

    /**
     *
     * @param model to the template
     * @param error  Login controls
     * @param logout Logout
     * @return  Login_template return
     */
    @GetMapping("/login")
    public String login(Model model, String error, String logout) {
        if (error != null)
            model.addAttribute("error", "Username et/ou password invalides.");

        if (logout != null)
            model.addAttribute("message", "Vous êtes connecté.");

        return "login";
    }

    /**
     *
     * @param model to the template
     * @return to welcome template
     */
    @GetMapping({"/", "/welcome"})
    public String welcome(Model model) {
        SecurityContext ctx = SecurityContextHolder.getContext();
        model.addAttribute("ctx", ctx);
        return "welcome";
    }


    @GetMapping( "/password/edit")
    public String passwordReset(Principal principal, Model model) {
        SecurityContext ctx = SecurityContextHolder.getContext();
        User myUser = UserRepository.findByUsername(principal.getName());

        model.addAttribute("userForm", myUser);
        model.addAttribute("ctx", ctx);
        return "users/edit";
    }

    /**
     *
     * @param userForm  userForm by registration
     * @param bindingResult  Form control
     * @return  go back to welcome
     */
    @PostMapping("/password/edit")
    public String passwordReset(@ModelAttribute("userForm") User userForm,
                                BindingResult bindingResult,
                                Principal principal) throws Exception {
        UserValidator.passwordValidate(userForm, bindingResult);
        if (bindingResult.hasErrors()) {
            return "users/edit";
        }
        User user = UserRepository.findByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName());
        user.setPassword(userForm.getPassword());
        UserService.changeUserPassword(user);

        return "redirect:/welcome";
    }

    /**
     *
     * @param model to the template
     * @return Accueil template
     */
    @GetMapping("/accueil")
    public String accueil(Model model) {
        return "accueil";
    }


    /**
     *
     * @param model to template
     * @return to registration med template
     */
    @GetMapping("users/medIndex")
    public String newMed(Model model) {
        model.addAttribute("userForm", new User());
        return "users/medIndex";
    }

    /**
     *
     * @param userForm  userForm by registration
     * @param bindingResult  Form control
     * @return  go back to welcome
     */
    @PostMapping("users/newAdmin")
    public String newAdmin(@ModelAttribute("userForm") User userForm, BindingResult bindingResult) {
        UserValidator.adminsValidate(userForm, bindingResult);
        if (bindingResult.hasErrors()) {
            return "users/newAdmin";
        }
        userService.saveAdmin(userForm);
        securityService.autoLogin(userForm.getUsername(), userForm.getPasswordConfirm());

        return "redirect:/welcome";
    }

    /**
     *
     * @param userForm  userForm by registration
     * @param bindingResult  Form control
     * @return  go back to welcome
     */
    @PostMapping("users/newMed")
    public String newMed(@ModelAttribute("userForm") User userForm, BindingResult bindingResult, Principal principal) {
        SecurityContext ctx = SecurityContextHolder.getContext();
        User myUser = UserRepository.findByUsername(principal.getName());
        userForm.setEphadId(myUser.getEphadId());
        UserValidator.adminsValidate(userForm, bindingResult);
        if (bindingResult.hasErrors()) {
            return "users/newMed";
        }
        userService.saveMed(userForm);
        securityService.autoLogin(userForm.getUsername(), userForm.getPasswordConfirm());

        return "redirect:/welcome";
    }

    /**
     *
     * @param model to template
     * @return to registration med template
     */
    @GetMapping("users/newMed")
    public String newMedRegistration(Model model) {
        model.addAttribute("userForm", new User());
        return "users/newMed";
    }


    /**
     *
     * @param model to template
     * @return to registration admins template
     */
    @GetMapping("users/newAdmin")
    public String newAdmin(Model model) {

        List<Ephad> ephads = EphadRepository.findAll();

        model.addAttribute("ephads", ephads);
        model.addAttribute("userForm", new User());
        return "users/newAdmin";
    }

    /**
     *
     * @param model for template
     * @param principal to get session
     * @return medIndex
     */
    // Affichage de la liste complète
    @RequestMapping("users/adminIndex")
    @GetMapping("users/adminIndex")
    public String medIndexModel( Model model, Principal principal) {
        SecurityContext ctx = SecurityContextHolder.getContext();
        User myUser = UserRepository.findByUsername(principal.getName());

        List<User> adminUsers = UserRepository.findAllByRolesId_Id( 1);

        model.addAttribute("adminUsers", adminUsers);
        return "users/adminIndex";
    }

    /**
     *
     * @param id of the object
     * @return  template for object view
     */
    // Affichage d'un objet
    @Transactional
    @GetMapping("users/deleteMed/{id}")
    public String deleteModel(@PathVariable("id") long id, Principal principal, HttpServletRequest request) {
        User myUser = UserRepository.findByUsername(principal.getName());
        User user = UserRepository.findById(id);
        if(myUser.getEphadId() == user.getEphadId() && request.isUserInRole("ROLE_ADMIN")){

            UserRepository.deleteById(user.getId());
        }
        return "/welcome";
    }

    /**
     *
     * @param id of the object
     * @return  template for object view
     */
    // Affichage d'un objet
    @Transactional
    @GetMapping("users/deleteAdmin/{id}")
    public String deleteAdminModel(@PathVariable("id") long id, HttpServletRequest request) {
        User user = UserRepository.findById(id);
        if(request.isUserInRole("ROLE_SUPADMIN")){

            UserRepository.deleteById(id);
        }
        return "/welcome";
    }
}