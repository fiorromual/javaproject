package com.gendteam.project.Controller;

import com.gendteam.project.model.User;
import com.gendteam.project.model.VilleFrance;
import com.gendteam.project.repository.VilleFranceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.lang.reflect.Field;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
public class VilleFranceController {

    private final com.gendteam.project.repository.VilleFranceRepository VilleFranceRepository;
    private final com.gendteam.project.repository.UserRepository UserRepository;
    private VilleFrance myRequest;

    @Autowired
    public VilleFranceController(VilleFranceRepository VilleFranceRepository, com.gendteam.project.repository.UserRepository UserRepository) {
        this.VilleFranceRepository = VilleFranceRepository;
        this.UserRepository = UserRepository ;
    }

    /**
     *
     * @param pageable
     * @param request
     * @param villeFranceForm
     * @param model
     * @param principal
     * @return
     */
    // Affichage de la liste complète
    @RequestMapping("villefrance/index")
    @GetMapping("villefrance/index")
    public String indexVilleFrance(@PageableDefault(size = 6) Pageable pageable, HttpServletRequest request,
                                 VilleFrance villeFranceForm, Model model, Principal principal) {
        // Affichage de données dans la console
        SecurityContext ctx = SecurityContextHolder.getContext();
        //Object currentUser = ctx.getAuthentication().getPrincipal();


        User myUser = UserRepository.findByUsername(principal.getName());
        model.addAttribute("username", myUser);


        Page<VilleFrance> page = (Page<VilleFrance>) filter(pageable, model,request, villeFranceForm);

        Pager(model, page);

        model.addAttribute("page", page);
        model.addAttribute("villeFranceForm", villeFranceForm);
        return "villefrance/index";
    }

    /**
     *
     * @param id
     * @param model
     * @return
     */
    // Affichage d'un objet
    @GetMapping("villefrance/show/{id}")
    public String showModel(@PathVariable("id") long id, Model model) {
        VilleFrance villeFrance = (VilleFrance) VilleFranceRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Ville inconnue:" + id));
        Date maDate = new Date();

        model.addAttribute("maDate", maDate);
        model.addAttribute("villeFrance", villeFrance);
        return "villefrance/show";
    }

    /**
     *
     * @param id
     * @param model
     * @return
     */
    @GetMapping("villefrance/edit/{id}")
    public String editModel(@PathVariable("id") long id, Model model) {
        VilleFrance villeFrance = (VilleFrance) VilleFranceRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Ville inconnue:" + id));
        model.addAttribute("villeFrance", villeFrance);
        return "villefrance/edit";
    }

    /**
     *
     * @param id
     * @param villeFrance
     * @param result
     * @param model
     * @return
     */
    @PostMapping("villefrance/update/{id}")
    public String updateUser(@PathVariable("id") long id, @Valid VilleFrance villeFrance, BindingResult result, Model model) {
        if (result.hasErrors()) {
            System.out.println("Erreur Modification de la date :" + result.toString());
            villeFrance.setId(id);
            return "/villefrance/edit";
        }
        //VilleFrance villeFranceSource = new VilleFrance();
        System.out.println("Modification de la date :" + villeFrance.getVilleCreatedAt());
        VilleFrance villeFranceSource = (VilleFrance) VilleFranceRepository.findById(id).orElseThrow(()-> new IllegalArgumentException("Ville inconnue:" + id));
        try {
            VilleFrance villeFranceSave = (VilleFrance) copyFields(villeFranceSource, villeFrance);
            VilleFranceRepository.save(villeFranceSave);
            return "redirect:/villefrance/show/" + villeFranceSave.getId();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "/villefrance/edit";
    }


    /**
     *
     * @param model
     * @return
     */
    // Creation d'un objet
    @GetMapping("villefrance/isNew")
    public String isNewModel(Model model) {

        SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        VilleFrance villeFrance=new VilleFrance();
        model.addAttribute("user", SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        model.addAttribute("villeFrance", villeFrance);
        return "/villefrance/new";
    }


    /**
     *
     * @param villeFrance
     * @param result
     * @return
     */
    @PostMapping("villefrance/new")
    public String newVilleFrance(@Valid VilleFrance villeFrance, BindingResult result) {
        if (result.hasErrors()) {
            return "/villefrance/new";
        }

        VilleFranceRepository.save(villeFrance);
        return "redirect:/villefrance/show/" + villeFrance.getId();
    }


    /**
     *
     * @param source
     * @param destination
     * @return
     * @throws Exception
     */
    private Object copyFields(Object source, Object destination) throws Exception{
        Class sourceClass = source.getClass();
        Field[] fields = sourceClass.getDeclaredFields();
        Class destinationClass = destination.getClass();
        Field[] duplicateFields = destinationClass.getDeclaredFields();
        //if source class and destination class are same do
        for (Field field : fields) {
            field.setAccessible(true);
            for ( Field duplicateField : duplicateFields) {
                duplicateField.setAccessible(true);
                //check value
                Object value = field.get(source);
                Object destinationValue = duplicateField.get(destination);
                //Compare values and set
                //duplicateField.set(destination,field.get(source));
            }
        }
        return destination;
    }

    /**
     *
     * @param request
     * @param villeFranceForm
     * @return
     */
    private Object getSessionStatus(HttpServletRequest request, VilleFrance villeFranceForm){
        HttpSession session = request.getSession();
        // Des données sont en session
        Object myRequest = villeFranceForm;
        VilleFrance myRequestReset = new VilleFrance("", "");

        if (session.getAttribute("selectRequest") != null ) {
            myRequest = session.getAttribute("selectRequest");
        }
        // Des données nouvelles sont mises en session
        if( request.getParameter("filter_action") != null &&
                request.getParameter("filter_action").equals("filter")){
            session.setAttribute("selectRequest",villeFranceForm);
            myRequest = session.getAttribute("selectRequest");
        }
        // Reset est activé, les données de la session sont vidées
        if( request.getParameter("filter_action") != null &&
                request.getParameter("filter_action").equals("reset")){
            myRequest = myRequestReset;
            session.setAttribute("selectRequest",myRequest);
        }
        return myRequest;
    }

    /**
     *
     * @param pageable
     * @param model
     * @param request
     * @param villeFranceForm
     * @return
     */
    private Object filter(@PageableDefault(size = 6) Pageable pageable, Model model, HttpServletRequest request,
                          VilleFrance villeFranceForm){

        VilleFrance villeFranceFilter = (VilleFrance) getSessionStatus(request, villeFranceForm);

        if( (villeFranceFilter.getVilleNom()== null && villeFranceFilter.getVilleCodePostal()== null)
                || villeFranceFilter == null) {
            Page<VilleFrance> page = VilleFranceRepository.findAll(pageable);

            return page;
        }
        else{
            String villeNom = new String();
            String villeCodePostal = new String();
            villeNom = villeFranceFilter.getVilleNom();
            villeCodePostal = villeFranceFilter.getVilleCodePostal();

            Page<VilleFrance> page = VilleFranceRepository.findAllByVilleNomOrVilleCodePostal(villeNom, villeCodePostal, pageable);

            return page;
        }
    }

    /**
     *
     * @param model
     * @param page
     */
    private void Pager(Model model, Page page){
        int totalPages = page.getTotalPages();
        int begin = 0;
        int end = 0;
        int pageCourante = page.getNumber();

        if(totalPages ==0 ){
            begin = 0;
            end = 0;
        }
        else if(totalPages <6){
            begin = 0;
            end = totalPages-1;
        }
        else if(pageCourante +6 >= totalPages){
            begin = totalPages-7;
            end = totalPages-1;
        }
        else if(pageCourante<4){
            begin = 0 ;
            end = 7;
        }else{
            begin= pageCourante-3;
            end=pageCourante+3;
        }

        model.addAttribute("pagerBegin", begin );
        model.addAttribute("pagerEnd", end);
        model.addAttribute("totalPages", totalPages);
    }

    /**
     *
     * @param request
     * @param villeFranceForm
     * @return
     */
    private VilleFrance sessionRequest(HttpServletRequest request, VilleFrance villeFranceForm){
        HttpSession session = request.getSession();
        if( request.getAttribute("selectRequest") == null && villeFranceForm == null )
            return null ;
        else if(villeFranceForm != null ) {
            return (VilleFrance) request.getAttribute("selectRequest");
        }
        else{
            return (VilleFrance) request.getAttribute("selectRequest");
        }
    }

    /**
     *
     * @param request
     * @param binder
     */
    @InitBinder
    protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, null,  new CustomDateEditor(dateFormat, true));
    }
}