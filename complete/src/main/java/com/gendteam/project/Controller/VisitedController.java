package com.gendteam.project.Controller;

import com.gendteam.project.model.Ephad;
import com.gendteam.project.model.User;
import com.gendteam.project.model.Visited;
import com.gendteam.project.repository.VisitedRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.lang.reflect.Field;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
public class VisitedController {

    @Autowired
    private com.gendteam.project.Controller.EmailController EmailController;

    private final com.gendteam.project.repository.VisitedRepository VisitedRepository;
    private final com.gendteam.project.repository.EphadRepository EphadRepository;
    private final com.gendteam.project.repository.UserRepository UserRepository;
    private Visited myRequest;

    @Autowired
    public VisitedController(VisitedRepository VisitedRepository, com.gendteam.project.repository.EphadRepository EphadRepository,
                             com.gendteam.project.repository.UserRepository UserRepository) {
        this.VisitedRepository = VisitedRepository;
        this.EphadRepository = EphadRepository;
        this.UserRepository = UserRepository ;

    }

    // Affichage de la liste complète
    @RequestMapping("visited/index")
    @GetMapping("visited/index")
    public String indexVisited(@PageableDefault(size = 6) Pageable pageable, HttpServletRequest request,
                                   Visited visitedForm, Model model, Principal principal) {

        model.addAttribute("user", SecurityContextHolder.getContext().getAuthentication().getPrincipal());

        Page<Visited> page = (Page<Visited>) filter(pageable, model,request, visitedForm, principal);

        Pager(model, page);

        model.addAttribute("page", page);
        model.addAttribute("visitedForm", visitedForm);
        return "visited/index";
    }

    // Affichage d'un objet
    @GetMapping("visited/show/{id}")
    public String showModel(@PathVariable("id") long id, Model model) {
        Visited visited = (Visited) VisitedRepository.findById(id);
        model.addAttribute("visited", visited);
        return "visited/show";
    }


    @GetMapping("visited/edit/{id}")
    public String editModel(@PathVariable("id") long id, Model model) {
        Visited visited = (Visited) VisitedRepository.findById(id);
        model.addAttribute("visited", visited);
        return "visited/edit";
    }

    @PostMapping("visited/update/{id}")
    public String updateUser(@PathVariable("id") long id, @Valid Visited visited, BindingResult result, Model model) {
        if (result.hasErrors()) {
            System.out.println("Erreur Modification de la date :" + result.toString());
            visited.setId(id);
            return "/visited/edit";
        }
        Visited visitedSource = (Visited) VisitedRepository.findById(id);
        try {
            Visited visitedSave = (Visited) copyFields(visitedSource, visited);
            VisitedRepository.save(visitedSave);
            return "redirect:/visited/show/" + visitedSave.getId();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "/visited/edit";
    }


    // Affichage d'un objet
    @GetMapping("visited/isNew")
    public String isNewModel(Model model) {

        SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Visited visitedForm = new Visited();
        model.addAttribute("user", SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        model.addAttribute("visitedForm", visitedForm);
        return "/visited/new";
    }

    @PostMapping("visited/new")
    public String newVisited(@Valid Visited visited,
                             Principal principal,
                             BindingResult result) {

        if (result.hasErrors()) {
            return "/visited/new";
        }
        // Récupération de l'Ephad via l'user connecté
        User user = UserRepository.findByUsername(principal.getName());
        System.out.println(user.getId());
        Ephad ephad = user.getEphadId();
        System.out.println(ephad.getId());
        System.out.println(ephad);
        //Ephad ephad = (Ephad) EphadRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Ephad inconnu:" + id));
        visited.setAlive(true);
        visited.setEphadId(ephad);
        //Sauvegarde
        VisitedRepository.save(visited);
        return "redirect:/visited/index";
//        return "redirect:/visited/show/" + visited.getId();
    }


    private Object copyFields(Object source, Object destination) throws Exception{
        Class sourceClass = source.getClass();
        Field[] fields = sourceClass.getDeclaredFields();
        Class destinationClass = destination.getClass();
        Field[] duplicateFields = destinationClass.getDeclaredFields();
        //if source class and destination class are same do
        for (Field field : fields) {
            field.setAccessible(true);
            for ( Field duplicateField : duplicateFields) {
                duplicateField.setAccessible(true);
                //check value
                Object value = field.get(source);
                Object destinationValue = duplicateField.get(destination);
                //Compare values and set
                //duplicateField.set(destination,field.get(source));
            }
        }
        return destination;
    }

    private Object getSessionStatus(HttpServletRequest request, Visited visitedForm){
        HttpSession session = request.getSession();
        // Des données sont en session
        Object myRequest = visitedForm;
        Visited myRequestReset = new Visited("", "");

        if (session.getAttribute("selectRequestVisited") != null ) {
            myRequest = session.getAttribute("selectRequestVisited");
//            session.setAttribute("selectRequestPersist","En mémoire");
        }
        // Des données nouvelles sont mises en session
        if( request.getParameter("filter_action") != null &&
                request.getParameter("filter_action").equals("filter")){
            session.setAttribute("selectRequestVisited",visitedForm);
            myRequest = session.getAttribute("selectRequestVisited");
//            session.setAttribute("selectRequestPersist","Ajouté en mémoire");
        }
        // Reset est activé, les données de la session sont vidées
        if( request.getParameter("filter_action") != null &&
                request.getParameter("filter_action").equals("reset")){
//            model.addAttribute("search2", "reset validé");
            myRequest = myRequestReset;
            session.setAttribute("selectRequestVisited",myRequest);
        }
        return myRequest;
    }

    private Object filter(@PageableDefault(size = 6) Pageable pageable, Model model, HttpServletRequest request,
                          Visited visitedForm, Principal principal){

        User myUser = UserRepository.findByUsername(principal.getName());
        Ephad myEphad = myUser.getEphadId();

        Visited visitedFilter = (Visited) getSessionStatus(request, visitedForm);

        if( (visitedFilter.getLastName()== null && visitedFilter.getFirstName()== null)
                || visitedFilter == null) {
            if(myEphad != null) {
                Page<Visited> page = VisitedRepository.findAllByEphadId(myEphad, pageable);
                return page;
            }
            else{
                Page<Visited> page = VisitedRepository.findAll(pageable);
                return page;
            }


        }
        else{
            String lastName = new String();
            String firstName = new String();
            lastName = visitedFilter.getLastName();
            firstName = visitedFilter.getFirstName();

            Page<Visited> page = VisitedRepository.findAllByLastNameOrFirstName(lastName, firstName, pageable);

            return page;
        }
    }

    private void Pager(Model model, Page page){
        int totalPages = page.getTotalPages();
        int begin = 0;
        int end = 0;
        int pageCourante = page.getNumber();

        if(totalPages ==0 ){
            begin = 0;
            end = 0;
        }
        else if(totalPages <6){
            begin = 0;
            end = totalPages-1;
        }
        else if(pageCourante +6 >= totalPages){
            begin = totalPages-7;
            end = totalPages-1;
        }
        else if(pageCourante<4){
            begin = 0 ;
            end = 7;
        }else{
            begin= pageCourante-3;
            end=pageCourante+3;
        }

        model.addAttribute("pagerBegin", begin );
        model.addAttribute("pagerEnd", end);
        model.addAttribute("totalPages", totalPages);
    }

    private Visited sessionRequest(HttpServletRequest request, Visited visitedForm){
        HttpSession session = request.getSession();
        if( request.getAttribute("selectRequest") == null && visitedForm == null )
            return null ;
        else if(visitedForm != null ) {
            return (Visited) request.getAttribute("selectRequest");
        }
        else{
            return (Visited) request.getAttribute("selectRequest");
        }
    }

    @InitBinder
    protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, null,  new CustomDateEditor(dateFormat, true));
    }
}
