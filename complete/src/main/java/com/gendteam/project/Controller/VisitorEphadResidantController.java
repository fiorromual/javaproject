package com.gendteam.project.Controller;

import com.gendteam.project.model.*;
import com.gendteam.project.repository.EphadRepository;
import com.gendteam.project.repository.VisitedRepository;
import com.gendteam.project.repository.VisitorEphadResidantRepository;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Controller
public class VisitorEphadResidantController {

    private com.gendteam.project.Controller.EmailController EmailController;

    private final com.gendteam.project.service.UserService UserService;
    private final com.gendteam.project.validator.UserValidator UserValidator;
    private final com.gendteam.project.repository.UserRepository UserRepository;
    private final com.gendteam.project.repository.EphadRepository EphadRepository;
    private final com.gendteam.project.repository.VisitorEphadResidantRepository VisitorEphadResidantRepository;
    private final com.gendteam.project.repository.VisitedRepository VisitedRepository;
    private final com.gendteam.project.repository.EventRepository EventRepository;

    public final static double AVERAGE_RADIUS_OF_EARTH_KM = 6371;

    public VisitorEphadResidantController(com.gendteam.project.repository.EventRepository EventRepository,
                                          VisitedRepository VisitedRepository,
                                          VisitorEphadResidantRepository VisitorEphadResidantRepository,
                                          com.gendteam.project.service.UserService UserService,
                                          EphadRepository EphadRepository, com.gendteam.project.repository.UserRepository UserRepository,
                                          com.gendteam.project.validator.UserValidator UserValidator) {
        this.UserService = UserService;
        this.UserRepository = UserRepository ;
        this.UserValidator = UserValidator;
        this.EphadRepository = EphadRepository;
        this.VisitorEphadResidantRepository = VisitorEphadResidantRepository;
        this.VisitedRepository = VisitedRepository ;
        this.EventRepository = EventRepository;
        
    }

    // Affichage de la liste complète des visiteurs
    @RequestMapping("visitorEphadResidant/visitorIndex")
    @GetMapping("visitorEphadResidant/visitorIndex")
    public String visitorIndexModel(@PageableDefault(size = 6) Pageable pageable, HttpServletRequest request,
                                    VisitorEphadResidant visitorForm, Model model, Principal principal, Authentication authentication) {
        // Affichage de données dans la console
        SecurityContext ctx = SecurityContextHolder.getContext();
        User user = UserRepository.findByUsername(principal.getName());
        Ephad ephad = new Ephad();

        // Ephad de Admin et medecin
        if(request.isUserInRole("ROLE_ADMIN") ||
                request.isUserInRole("ROLE_MED")) {
            Long id = user.getEphadId().getId();
            ephad = (Ephad) EphadRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Ephad inconnu:" + id));
            Page<VisitorEphadResidant> page = (Page<VisitorEphadResidant>) filter(pageable, model,request, visitorForm, ephad, user, authentication);
            Pager(model, page);
            model.addAttribute("page", page);
        }else{
            Page<VisitorEphadResidant> page = VisitorEphadResidantRepository.findByUserId(user, pageable);
            visitorForm =null;
            Pager(model, page);
            model.addAttribute("page", page);
        }

        ObjectForm myForm= new ObjectForm();
        model.addAttribute("myForm", myForm);
        model.addAttribute("username", user);
        model.addAttribute("visitorForm", visitorForm);
        return "visitorEphadResidant/visitorIndex";
    }

    // Affichage de la liste complète des visiteurs
    @PostMapping("calendarVisited/index")
    public String calendarVisitedModel(ObjectForm myForm,
                                       HttpServletRequest request,
                                       VisitorEphadResidant visitorForm,
                                       Model model,
                                       Principal principal) {

        System.out.println("***********************************************valeur :" + myForm);
        // Affichage de données dans la console
        SecurityContext ctx = SecurityContextHolder.getContext();
        User myUser = UserRepository.findByUsername(principal.getName());
        Visited visitedId = VisitedRepository.findById(myForm.getObject1());

        model.addAttribute("visitedId", visitedId);
        model.addAttribute("username", myUser);
        return "calendar/index";
    }

    @GetMapping("visitorEphadResidant/edit/{id}")
    public String editModel(@PathVariable("id") long id, Model model, Principal principal) {
        VisitorEphadResidant visitorEphadResidant = (VisitorEphadResidant) VisitorEphadResidantRepository.findById(id);

        SecurityContext ctx = SecurityContextHolder.getContext();
        User myUser = UserRepository.findByUsername(principal.getName());
        User user = UserRepository.findByUsername(myUser.getUsername());
        Long userid = user.getEphadId().getId();
        Ephad ephad = (Ephad) EphadRepository.findById(userid).orElseThrow(() -> new IllegalArgumentException("Ephad inconnu:" + userid));
        List<Visited> visited = VisitedRepository.findAllByEphadId(ephad);

        ObjectForm  myForm= new ObjectForm();
        myForm.setObject1(id);

        model.addAttribute("myForm", myForm);
        model.addAttribute("visited", visited);
        model.addAttribute("visitorEphadResidant", visitorEphadResidant);
        return "visitorEphadResidant/edit";
    }

    @PostMapping("visitorEphadResidant/update")
    public String updateVisitorEphadResidant(VisitorEphadResidant visitorEphadResidant,
                             ObjectForm myForm,
                             Model model) throws IOException, MessagingException {

        System.out.println("resultat" + myForm.toString());
        //Récupération de l'id
        visitorEphadResidant = VisitorEphadResidantRepository.findById(myForm.getObject1());
        // Récupération de l'objet visited
        Visited visited = VisitedRepository.findById(myForm.getObject2());

        visitorEphadResidant.setVisitedId(visited);
        VisitorEphadResidantRepository.save(visitorEphadResidant);
        EmailController.sendmail(
                visitorEphadResidant.getUserId().getEmail(),
                "Demande de visite M ou Mme" + visitorEphadResidant.getVisitedId().getFirstName(),
                visitorEphadResidant.getEphadId().getRaisonSociale()+" "+
                        "Bonjour, votre demande de droits de visites a été validée. Vous pouvez créer des visites"
                );

        return "redirect:/visitorEphadResidant/visitorIndex";
    }

    // Creation d'un objet
    @GetMapping("visitorEphadResidant/newMeeting/{id}")
    public String newMeetingModel(@PathVariable("id") long id, Model model) {
        // L'id récupéré est l'id de la relation visiteur résidant
        ObjectForm myForm = new ObjectForm();
        VisitorEphadResidant visitorResidant  = VisitorEphadResidantRepository.findById(id);
        myForm.setObject1(id);
        System.out.println("**************** entree valeur visitorEphadResidant/newMeeting/id :" + myForm);
        model.addAttribute("visitorResidant", visitorResidant);
        model.addAttribute("myForm", myForm);
        return "/visitorEphadResidant/newMeeting";

    }

    // Creation d'un objet
    @Transactional
    @PostMapping("visitorEphadResidant/newMeeting")
    public String newMeetingCreateModel(@Valid ObjectForm myForm, BindingResult result, Principal principal
                                       ) throws IOException, MessagingException {

        // objectForm.object1 est l'id de la relation visitor-residant
        System.out.println("**************** entrée valeur visitorEphadResidant/newMeeting :" + myForm);
        if (result.hasErrors()) {
            System.out.println("**************** valeur hasError visitorEphadResidant/newMeeting :" + myForm);
            return "visitorEphadResidant/newMeeting/" + myForm.getObject1();
        }
//        **************************************************
//        ********** Informations de Event *****************
//        ***
//        **************************************************
        Event event = new Event();
        // Implémentation de l'event

        // Récupération de l'user connecté
        SecurityContext ctx = SecurityContextHolder.getContext();
        User myUser = UserRepository.findByUsername(principal.getName());
        event.setUserId(myUser.getId());
        //Récupération du résidant
        VisitorEphadResidant visitorResidant  = VisitorEphadResidantRepository.findById(myForm.getObject1());
        Visited visited = visitorResidant.getVisitedId();
        System.out.println("**************** valeur residant :" + visited);
        event.setVisitedId(visited);

//        **************************************************
//        ********** Informations de Event *****************
//        ***  event.color
//        **************************************************
        // Gestion de la couleur du calendrier
        if(visitorResidant.getRelation()==2){
            event.setColor("#6aa84f");
        }

//        **************************************************
//        ********** Informations de Event *****************
//        ***  event.Relation
//        ***   event.EphadId
//        **************************************************
        //Ephad ephad = EphadRepository.findOneById(visited.getEphadId().getId());
        event.setEphadId(visited.getEphadId());
        event.setRelation(visitorResidant.getRelation());

        //Ajout de l'ephad pour le medecin (plus rapide en execution)

//        **************************************************
//        ********** Informations de Event *****************
//        ***  event.Start
//        ***   event.End
//        **************************************************

        // Modification date de debut et de fin
        Date maDateDebut = eventDate(myForm.getDateMeeting(),myForm.getHour());
        System.out.println("Date de debut" + maDateDebut);
        Date maDateFin = eventDate(maDateDebut,2);
        System.out.println("Date de fin" + maDateFin);
        event.setStart(convertToLocalDateTimeViaInstant(maDateDebut));
        event.setEnd(convertToLocalDateTimeViaInstant(maDateFin));
        System.out.println("Date de fin" + event.getEnd());


//        **************************************************
//        ********** Informations de Event *****************
//        ***  event.Text
//        ***
//        **************************************************
        // Modification du commentaire
        if(myForm.getMedical()==1 && myForm.getRencontres()==1){
            event.setText("Demande de visite conforme sous réserve de validation du médecin");
        }else{
            event.setText("Contact préliminaire requis avec le médecin de l'Ephad ");
        }
        // Ajout des infos complementaires
        event.setInformations(myForm.getComment());


//        **************************************************
//        ********** Prérequis de validation ***************
//        ***
//        **************************************************

        // Vérification des prerequis
        // Capacité de l'Ephad
        Boolean validCapacity = capacityVerif(event, visited);
        System.out.println("**************** capacite :" + validCapacity);

        // Vérification de la distance de visite
        int distance = calculateDistanceInKilometer(myUser.getVilleFranceId().getVilleLatitudeDeg(),
                myUser.getVilleFranceId().getVilleLongitudeDeg(),
                visited.getEphadId().getLatitude(),
                visited.getEphadId().getLongitude());
        System.out.println("**************** distance :" + distance);

        //Vérification si une association a déjà le créneau.
        int assoHasAMeeting = EventRepository.countByStartAndVisitedIdAndRelation(event.getStart(),visited, 2);
        int existingFamilyMeeting = EventRepository.countByStartAndVisitedIdAndRelation(event.getStart(), visited, 1);
        System.out.println("**************** existance d'une demande de famille :" + existingFamilyMeeting);
        System.out.println("**************** existance d'une demande d'assoc :" + assoHasAMeeting);

        // Destuction de ce créneau et envoi d'un mail à l'association
        if(assoHasAMeeting >0 ) {

            // Le choix de l'équipe est de donner la décision au gestionnaire
            Event assoMeeting = EventRepository.findByStartAndVisitedId(event.getStart(), visited);
            System.out.println("**************** objet de l'existance d'une visite association :" + assoMeeting);

            //              EventRepository.deleteById(assoMeeting.getId());
            if(visitorResidant.getRelation() == 1 ){
                validCapacity = true;
            }

            int assoHasNoMeeting = EventRepository.countByStartAndVisitedIdAndRelation(event.getStart(),visited, 2);
            System.out.println("**************** existance d'une demande d'assoc :" + assoHasNoMeeting);
            User assoUser = UserRepository.findById(assoMeeting.getUserId());
            EmailController.sendmail(
                    assoUser.getEmail(),
                    "Demande de visite M ou Mme" + event.getVisitedId().getFirstName(),
                    event.getEphadId().getRaisonSociale()+" "+
                            "Bonjour, votre demande de visite est susceptible d'être annuléee. " +
                            "Un membre de la famille a réservé votre créneau"
                    );
        }

        // La capacité n'est pas depassée, ou la création remplace celle d'une association et la famille n'a pas déposé de demande
        if((validCapacity == true || assoHasAMeeting>0) && existingFamilyMeeting == 0) {
            EventRepository.save(event);
            // Modification du commentaire
            if (myForm.getMedical() == 1 && myForm.getRencontres() == 1) {
                event.setText("N°" + event.getId() + ".Demande de visite conforme sous réserve de validation du médecin");
                if (distance > 100) {
                    event.setText("N°" + event.getId() + ".Demande de visite conforme sous réserve de validation du médecin. " +
                            "Attention vous dépassez la distance de 100 km.");
                }
            } else {
                event.setText("N°" + event.getId() + ".Contact préliminaire requis avec le médecin de l'Ephad ");
                if (distance > 100) {
                    event.setText("N°" + event.getId() + ".Contact préliminaire requis avec le médecin de l'Ephad " +
                            "Attention vous dépassez la distance de 100 km.");
                }
            }

//        **************************************************
//        ********** Informations de Event *****************
//        ***  Sauvegarde et Maj de event.Text
//        **************************************************
            EventRepository.save(event);
            EmailController.sendmail(
                    myUser.getEmail(),
                    "Demande de visite M ou Mme" + event.getVisitedId().getFirstName(),
                    event.getEphadId().getRaisonSociale()+" "+
                            "Bonjour, votre demande de visite a été enregistrée. Elle est en cours de traitement"
                    );
            return "redirect:/welcome";
        }
        
        return "redirect:/visitorEphadResidant/errorCreateMeeting";
    }


    /**
     *
     * @param model
     * @return page d'erreur de création d'une visite
     */
    // Page d'erreur
    @GetMapping("visitorEphadResidant/errorCreateMeeting")
    public String errorMeetingModel(Model model) {

        return "/visitorEphadResidant/errorCreateMeeting";

    }

    public Date eventDate(Date date, int hours) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.HOUR_OF_DAY, hours);
        return calendar.getTime();
    }

    private Boolean capacityVerif(Event event, Visited visited){
        Integer capacity = visited.getEphadId().getCapaciteVisite();
        Integer visits = EventRepository.countAllByStartEqualsAndEphadId(event.getStart(), visited.getEphadId());
        if(visits == null) visits = 0;
        if(capacity == null) capacity = 0;


        return capacity >= visits;
    }



    public int calculateDistanceInKilometer(double userLat, double userLng,
                                            double venueLat, double venueLng) {

        double latDistance = Math.toRadians(userLat - venueLat);
        double lngDistance = Math.toRadians(userLng - venueLng);

        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(userLat)) * Math.cos(Math.toRadians(venueLat))
                * Math.sin(lngDistance / 2) * Math.sin(lngDistance / 2);

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return (int) (Math.round(AVERAGE_RADIUS_OF_EARTH_KM * c));
    }

    // Conversion Date LocalDateTime
    public LocalDateTime convertToLocalDateTimeViaInstant(Date dateToConvert) {
        return dateToConvert.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
    }


    // Creation user Family ou Association
    @GetMapping("ephad/visitorCreate/{id}")
    public String visitorCreateModel(@PathVariable("id") long id, Model model) {
        Ephad ephad = (Ephad) EphadRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Ephad inconnu:" + id));
        VisitorEphadResidant userForm = new VisitorEphadResidant();
        model.addAttribute("userForm", userForm);
        model.addAttribute("ephad", ephad);
        return "users/newFamilyAssociation";
    }

    @PostMapping("ephad/visitorCreate/{id}")
    public String visitorCreateModel(@PathVariable("id") long id,
                                     @ModelAttribute("userForm") VisitorEphadResidant userForm,
                                     BindingResult bindingResult,
                                     Principal principal,
                                     Model model) {
        //Récupération de l'Ephad
        Ephad ephad = (Ephad) EphadRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Ephad inconnu:" + id));
        if (bindingResult.hasErrors()) {

            model.addAttribute("ephad", ephad);
            return "users/newFamilyAssociation";
        }

        User user = UserRepository.findByUsername(principal.getName());
        userForm.setUserId(user);
        userForm.setEphadId(ephad);
        VisitorEphadResidantRepository.save(userForm);

        return "redirect:/welcome";
    }

    private Object getSessionStatus(HttpServletRequest request, VisitorEphadResidant visitorEphadResidantForm){
        HttpSession session = request.getSession();
        // Des données sont en session
        Object myRequest = visitorEphadResidantForm;
        VisitorEphadResidant myRequestReset = new VisitorEphadResidant( new User());

        if (session.getAttribute("selectRequest") != null ) {
            myRequest = session.getAttribute("selectRequest");
//            session.setAttribute("selectRequestPersist","En mémoire");
        }
        // Des données nouvelles sont mises en session
        if( request.getParameter("filter_action") != null &&
                request.getParameter("filter_action").equals("filter")){
            session.setAttribute("selectRequest",visitorEphadResidantForm);
            myRequest = session.getAttribute("selectRequest");
//            session.setAttribute("selectRequestPersist","Ajouté en mémoire");
        }
        // Reset est activé, les données de la session sont vidées
        if( request.getParameter("filter_action") != null &&
                request.getParameter("filter_action").equals("reset")){
//            model.addAttribute("search2", "reset validé");
            myRequest = myRequestReset;
            session.setAttribute("selectRequest",myRequest);
        }
        return myRequest;
    }

    private Object filter(@PageableDefault(size = 6) Pageable pageable, Model model, HttpServletRequest request,
                          VisitorEphadResidant visitorEphadResidantForm, Ephad ephad, User user,  Authentication authentication){

        //VisitorEphadResidant visitorEphadResidantFilter = (VisitorEphadResidant) getSessionStatus(request, visitorEphadResidantForm);

        if(request.isUserInRole("ROLE_ADMIN") || request.isUserInRole("ROLE_MED")) {
            Page<VisitorEphadResidant> page = VisitorEphadResidantRepository.findAllByEphadId( ephad, pageable);

            return page;

//        else if( user.getRole().getId()==1 || user.getRole().getId()==3) {
//            User userId = new User();
//            userId = visitorEphadResidantFilter.getUserId();
//
//            Page<VisitorEphadResidant> page = VisitorEphadResidantRepository.findByUserId(userId, pageable);
//
//            return page;
        } else {
            // Utilisateur standard qui doit voir ses résidants à visiter
            Page<VisitorEphadResidant> page = VisitorEphadResidantRepository.findByUserId(user, pageable);

            return page;
        }
    }

    private void Pager(Model model, Page page){
        int totalPages = page.getTotalPages();
        int begin = 0;
        int end = 0;
        int pageCourante = page.getNumber();

        if(totalPages ==0 ){
            begin = 0;
            end = 0;
        }
        else if(totalPages <6){
            begin = 0;
            end = totalPages-1;
        }
        else if(pageCourante +6 >= totalPages){
            begin = totalPages-7;
            end = totalPages-1;
        }
        else if(pageCourante<4){
            begin = 0 ;
            end = 7;
        }else{
            begin= pageCourante-3;
            end=pageCourante+3;
        }

        model.addAttribute("pagerBegin", begin );
        model.addAttribute("pagerEnd", end);
        model.addAttribute("totalPages", totalPages);
    }

    private VisitorEphadResidant sessionRequest(HttpServletRequest request, VisitorEphadResidant visitorEphadResidantForm){
        HttpSession session = request.getSession();
        if( request.getAttribute("selectRequest") == null && visitorEphadResidantForm == null )
            return null ;
        else if(visitorEphadResidantForm != null ) {
            return (VisitorEphadResidant) request.getAttribute("selectRequest");
        }
        else{
            return (VisitorEphadResidant) request.getAttribute("selectRequest");
        }
    }

    /**
     *
     * @param request
     * @param binder
     */
    @InitBinder
    protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, null,  new CustomDateEditor(dateFormat, true));
    }

//    /**
//     *
//     * @return
//     * @throws AddressException
//     * @throws MessagingException
//     * @throws IOException
//     */
//    @RequestMapping(value = "/sendemail")
//    public String sendEmail() throws AddressException, MessagingException, IOException {
//        // On inject tous les paramètres nécessaires
//        EmailController.sendmail("","","","");
//        return "Email envoyé avec succès";
//    }

}

