package com.gendteam.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

//@EnableAutoConfiguration
//@ComponentScan(basePackages={"com.gendteam.project"})
//@EnableJpaRepositories(basePackages="com.gendteam.project.repository")
//@EnableTransactionManagement
//@EntityScan(basePackages="com.gendteam.project.model")
//
//
//@SpringBootApplication
//public class GendTeamApplication {
//
//	public static void main(String[] args) {
//		SpringApplication.run(GendTeamApplication.class, args);
//	}
//}

@EntityScan(
		basePackageClasses = { GendTeamApplication.class, Jsr310JpaConverters.class }
)
@SpringBootApplication
public class GendTeamApplication {
	public static void main(String[] args) throws Exception {
		SpringApplication.run(GendTeamApplication.class, args);
	}
}