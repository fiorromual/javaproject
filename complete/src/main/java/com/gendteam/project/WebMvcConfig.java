package com.gendteam.project;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class WebMvcConfig extends WebMvcConfigurerAdapter{

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/login").setViewName("login");
		registry.addViewController("/welcome").setViewName("welcome");
		registry.addViewController("/").setViewName("login");
		registry.addViewController("/registration").setViewName("registration");

		//Essai
		registry.addViewController("/essais").setViewName("essais/index");
		registry.addViewController("/thymleaf").setViewName("essais/view");

		//VilleFrance
		registry.addViewController("/villefrance/index").setViewName("villefrance/index");
		registry.addViewController("/villefrance/show").setViewName("villefrance/show");
		registry.addViewController("/villefrance/edit").setViewName("villefrance/edit");
		registry.addViewController("/villefrance/isNew").setViewName("villefrance/new");
		registry.addViewController("/villefrance/new").setViewName("villefrance/new");

		//Ephad
		registry.addViewController("/ephad/index").setViewName("ephad/index");
		registry.addViewController("/ephad/show").setViewName("ephad/show");
//		registry.addViewController("/ephad/visitorCreate").setViewName("ephad/visitorCreate");

		//Visited
		registry.addViewController("/visited/index").setViewName("visited/index");
		registry.addViewController("/visited/show").setViewName("visited/show");
		registry.addViewController("/visited/edit").setViewName("visited/edit");

		//Calendrier
		registry.addViewController("/calendar/index").setViewName("calendar/index");

		//users
//		registry.addViewController("/ephad/visitorCreate").setViewName("users/newFamilyAssociation");

	}






}
