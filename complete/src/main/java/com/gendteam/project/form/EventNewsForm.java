package com.gendteam.project.form;

import com.gendteam.project.model.VilleFrance;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

public class EventNewsForm extends VilleFrance {

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Temporal(TemporalType.DATE) //type (DATE, TIME ou TIMESTAMP)

    private Date dateNews;

    public Date getDateNews() {
        return dateNews;
    }

    public void setDateNews(Date dateNews) {
        this.dateNews = dateNews;
    }
}