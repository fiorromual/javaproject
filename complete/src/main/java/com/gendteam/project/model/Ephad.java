package com.gendteam.project.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "ephad", schema = "springlabs")
public class Ephad implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id // Pour la base de données
    @GeneratedValue(strategy = GenerationType.AUTO) // Pour la base de données
    @Column(name = "id") // Pour la base de données
    private Long id;

    @Column(name = "nofinesset", length = 12)
    private String nofinesset;

    @Column(name = "raison_sociale", length = 255)
    private String raisonSociale;

    @Column(name = "telephone", length = 15)
    private String telephone;

    @Column(name = "site", length = 45)
    private String site;

    @Column(name = "courriel", length = 75)
    private String courriel;

    @Column(name = "gestionnaire", length = 255)
    private String gestionnaire;

    @Column(name = "statut_juridique", length = 255)
    private String statutJuridique;

    @Column(name = "capacite_totale")
    private Integer capaciteTotale;

    @Column(name = "capacite_complet")
    private Integer capaciteComplet;

    @Column(name = "capacite_visite")
    private Integer capaciteVisite;

    @Column(name = "adresse", length = 255)
    private String adresse;

    @Column(name = "code_postal", length = 5)
    private String codePostal;

    @Column(name = "ville", length = 50)
    private String ville;

    @Column(name = "longitude")
    private double longitude;

    @Column(name = "latitude")
    private double latitude;

    // Constructeur

    public Ephad() {
    }

    public Ephad(String ville, String codePostal) {
    }


    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNofinesset() {
        return nofinesset;
    }

    public void setNofinesset(String nofinesset) {
        this.nofinesset = nofinesset;
    }

    public String getRaisonSociale() {
        return raisonSociale;
    }

    public void setRaisonSociale(String raisonSociale) {
        this.raisonSociale = raisonSociale;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getCourriel() {
        return courriel;
    }

    public void setCourriel(String courriel) {
        this.courriel = courriel;
    }

    public String getGestionnaire() {
        return gestionnaire;
    }

    public void setGestionnaire(String gestionnaire) {
        this.gestionnaire = gestionnaire;
    }

    public String getStatutJuridique() {
        return statutJuridique;
    }

    public void setStatutJuridique(String statutJuridique) {
        this.statutJuridique = statutJuridique;
    }

    public Integer getCapaciteTotale() {
        return capaciteTotale;
    }

    public void setCapaciteTotale(Integer capaciteTotale) {
        this.capaciteTotale = capaciteTotale;
    }

    public Integer getCapaciteComplet() {
        return capaciteComplet;
    }

    public void setCapaciteComplet(Integer capaciteComplet) {
        this.capaciteComplet = capaciteComplet;
    }

    public Integer getCapaciteVisite() {
        return capaciteVisite;
    }

    public void setCapaciteVisite(Integer capaciteVisite) {
        this.capaciteVisite = capaciteVisite;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }
}