package com.gendteam.project.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class Event {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	Long id;

	@Column(name = "text", length = 255)
	String text;

	@Column(name = "informations", length = 255)
	String informations;

	// 1 pour famille, 2 pour association... Simplification de recherche
	@Column(name = "relation")
	Integer relation;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "visited.id")
	private Visited visitedId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ephad.id")
	private Ephad ephadId;

//	@ManyToOne(fetch = FetchType.EAGER)
//	@JoinColumn(name = "user.id")
//	private User userId;

	@Column(name = "user_id")
	Long userId;

	LocalDateTime start;
	
	LocalDateTime end;
	
	@ManyToOne
	@JsonIgnore
	Resource resource;

	String color;



	public Event() {
	}

	@JsonProperty("resource")
	public Long getResourceId() {
		return resource != null ? resource.getId() : null;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Visited getVisitedId() {
		return visitedId;
	}

	public void setVisitedId(Visited visitedId) {
		this.visitedId = visitedId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Ephad getEphadId() {
		return ephadId;
	}

	public void setEphadId(Ephad ephadId) {
		this.ephadId = ephadId;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public LocalDateTime getStart() {
		return start;
	}

	public void setStart(LocalDateTime start) {
		this.start = start;
	}

	public Integer getRelation() {
		return relation;
	}

	public void setRelation(Integer relation) {
		this.relation = relation;
	}

	public LocalDateTime getEnd() {
		return end;
	}

	public void setEnd(LocalDateTime end) {
		this.end = end;
	}

	public Resource getResource() {
		return resource;
	}

	public void setResource(Resource resource) {
		this.resource = resource;
	}

	public String getColor() { return color; }

	public void setColor(String color) { this.color = color; }

	public String getInformations() {
		return informations;
	}

	public void setInformations(String informations) {
		this.informations = informations;
	}
}
