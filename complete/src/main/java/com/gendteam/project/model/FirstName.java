package com.gendteam.project.model;

import javax.persistence.*;

@Entity
@Table(name = "first_name", schema = "public")
public class FirstName {


    private static final long serialVersionUID = 1L;

    @Id // Pout la base de données
    @GeneratedValue(strategy = GenerationType.AUTO) // Pout la base de données
    @Column(name = "id")  // Pout la base de données
    private Long id;

    @Column(name = "first_name", length = 75)
    private String FirstName;

    public FirstName() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }
}
