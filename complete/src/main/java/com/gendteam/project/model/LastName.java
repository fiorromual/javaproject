package com.gendteam.project.model;

import javax.persistence.*;

@Entity
@Table(name = "last_name", schema = "public")
public class LastName {


    private static final long serialVersionUID = 1L;

    @Id // Pout la base de données
    @GeneratedValue(strategy = GenerationType.AUTO) // Pout la base de données
    @Column(name = "id")  // Pout la base de données
    private Long id;

    @Column(name = "last_name", length = 75)
    private String LastName;

    public LastName() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }
}
