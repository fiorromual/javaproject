package com.gendteam.project.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "objectform", schema = "public")
public class ObjectForm implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "object1")
    private Long object1;

    @Column(name = "object2")
    private Long object2;

    @Column(name = "object3")
    private Long object3;

    @Column(name = "object4")
    private Long object4;

    @Column(name = "object5")
    private Long object5;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Temporal(TemporalType.DATE) //type (DATE, TIME ou TIMESTAMP)
    @Column(name = "date_meeting")
    private Date dateMeeting;

    @Column(name = "comment", length = 255)
    private String comment;

    @Column(name = "hour")
    private Integer hour;

    @Column(name = "medical")
    private Integer medical;

    @Column(name = "rencontres")
    private Integer rencontres;

    public ObjectForm() { ;
    }

    public ObjectForm(Long id, String comment) {
        this.id = id;
        this.comment = comment;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getObject1() {
        return object1;
    }

    public void setObject1(Long object1) {
        this.object1 = object1;
    }

    public Long getObject2() {
        return object2;
    }

    public void setObject2(Long object2) {
        this.object2 = object2;
    }

    public Long getObject3() {
        return object3;
    }

    public void setObject3(Long object3) {
        this.object3 = object3;
    }

    public Long getObject4() {
        return object4;
    }

    public void setObject4(Long object4) {
        this.object4 = object4;
    }

    public Long getObject5() {
        return object5;
    }

    public void setObject5(Long object5) {
        this.object5 = object5;
    }

    public Date getDateMeeting() {
        return dateMeeting;
    }

    public void setDateMeeting(Date dateMeeting) {
        this.dateMeeting = dateMeeting;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getHour() {
        return hour;
    }

    public void setHour(Integer hour) {
        this.hour = hour;
    }

    public Integer getMedical() {
        return medical;
    }

    public void setMedical(Integer medical) {
        this.medical = medical;
    }

    public Integer getRencontres() {
        return rencontres;
    }

    public void setRencontres(Integer rencontres) {
        this.rencontres = rencontres;
    }

    @Override
    public String toString() {
        return "ObjectForm{" +
                "id=" + id +
                ", object1=" + object1 +
                ", object2=" + object2 +
                ", object3=" + object3 +
                ", object4=" + object4 +
                ", object5=" + object5 +
                '}';
    }
}