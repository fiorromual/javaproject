package com.gendteam.project.model;

import org.hibernate.validator.constraints.Email;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "user", schema = "public")
public class User implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id // Pout la base de données
	@GeneratedValue(strategy = GenerationType.AUTO) // Pout la base de données
	@Column(name = "id")  // Pout la base de données
	private Long id;

	@Transient
	private String passwordConfirm;

	@Column(name = "first_name", length = 75)
	private String firstName;

	@Column(name = "last_name", length = 80)
	private String lastName;

	@Column(name = "username",unique = true, length = 65)
	private String username;

	@Column(name = "password", length = 64)
	private String password;

	@Email(message = "L'Email doit être valide")
	@Column(name = "email", unique = true, length = 115)
	private String email;

	@Column(name = "relation")
	private Long relation;

	@Column(name = "comment", length = 400)
	private String comment;

	@ManyToMany
	private Set<Role> roles;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ephad.id")
	private Ephad ephadId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "villefrance.id")
	private VilleFrance villeFranceId;

	public User() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Long getRelation() {
		return relation;
	}

	public void setRelation(Long relation) {
		this.relation = relation;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Ephad getEphadId() {
		return ephadId;
	}

	public void setEphadId(Ephad ephadId) {
		this.ephadId = ephadId;
	}

	public String getPasswordConfirm() {
		return passwordConfirm;
	}

	public void setPasswordConfirm(String passwordConfirm) {
		this.passwordConfirm = passwordConfirm;
	}

	public VilleFrance getVilleFranceId() {
		return villeFranceId;
	}

	public void setVilleFranceId(VilleFrance villeFranceId) {
		this.villeFranceId = villeFranceId;
	}
}
