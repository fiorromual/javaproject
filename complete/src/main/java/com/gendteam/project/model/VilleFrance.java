package com.gendteam.project.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "villefrance", schema = "springlabs")
public class VilleFrance implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id // Pour la base de données
    @GeneratedValue(strategy = GenerationType.AUTO) // Pour la base de données
    @Column(name = "id") // Pour la base de données
    private Long id;

    @Column(name = "ville_departement", length = 3)
    private String villeDepartement;

    @Column(name = "ville_slug", length = 255)
    private String villeSlug;

    @Size(min = 3, max = 45) // Validateur ou length (Hibernate)
    @Column(name = "ville_nom", length = 45)
    private String villeNom;

    @Column(name = "ville_nom_simple", length = 74)
    private String villeNomSimple;

    @Column(name = "ville_nom_reel", length = 45)
    private String villeNomReel;

    @Column(name = "ville_nom_soundex", length = 20)
    private String villeNomSoundex;

    @Column(name = "ville_nom_metaphone", length = 22)
    private String villeNomMetaphone;

    @Column(name = "ville_code_postal", length = 255)
    private String villeCodePostal;

    @Column(name = "ville_Commune", length = 3)
    private String villeCommune;

    @Column(name = "ville_arrondissement")
    private Integer villeArrondissement;

    @Column(name = "ville_canton", length = 4)
    private String villeCanton;

    @Column(name = "ville_amdi", length = 5)
    private Integer villeAmdi;

    @Column(name = "ville_surface")
    private Long villeSurface;

    @Column(name = "ville_longitude_deg")
    private double villeLongitudeDeg;

    @Column(name = "ville_latitude_deg")
    private double villeLatitudeDeg;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Temporal(TemporalType.DATE) //type (DATE, TIME ou TIMESTAMP)
    @Column(name = "ville_Created_At")
    private Date villeCreatedAt;

    // Constructeur
    public VilleFrance() {
    }

    public VilleFrance(String villeNom, String villeCodePostal) {
    }


    // Getter et setter
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVilleDepartement() {
        return villeDepartement;
    }

    public void setVilleDepartement(String villeDepartement) {
        this.villeDepartement = villeDepartement;
    }

    public String getVilleSlug() {
        return villeSlug;
    }

    public void setVilleSlug(String villeSlug) {
        this.villeSlug = villeSlug;
    }

    public String getVilleNom() {
        return villeNom;
    }

    public void setVilleNom(String villeNom) {
        this.villeNom = villeNom;
    }

    public String getVilleNomSimple() {
        return villeNomSimple;
    }

    public void setVilleNomSimple(String villeNomSimple) {
        this.villeNomSimple = villeNomSimple;
    }

    public String getVilleNomReel() {
        return villeNomReel;
    }

    public void setVilleNomReel(String villeNomReel) {
        this.villeNomReel = villeNomReel;
    }

    public String getVilleNomSoundex() {
        return villeNomSoundex;
    }

    public void setVilleNomSoundex(String villeNomSoundex) {
        this.villeNomSoundex = villeNomSoundex;
    }

    public String getVilleNomMetaphone() {
        return villeNomMetaphone;
    }

    public void setVilleNomMetaphone(String villeNomMetaphone) {
        this.villeNomMetaphone = villeNomMetaphone;
    }

    public String getVilleCodePostal() {
        return villeCodePostal;
    }

    public void setVilleCodePostal(String villeCodePostal) {
        this.villeCodePostal = villeCodePostal;
    }

    public String getVilleCommune() {
        return villeCommune;
    }

    public void setVilleCommune(String villeCommune) {
        this.villeCommune = villeCommune;
    }

    public Integer getVilleArrondissement() {
        return villeArrondissement;
    }

    public void setVilleArrondissement(Integer villeArrondissement) {
        this.villeArrondissement = villeArrondissement;
    }

    public String getVilleCanton() {
        return villeCanton;
    }

    public void setVilleCanton(String villeCanton) {
        this.villeCanton = villeCanton;
    }

    public Integer getVilleAmdi() {
        return villeAmdi;
    }

    public void setVilleAmdi(Integer villeAmdi) {
        this.villeAmdi = villeAmdi;
    }

    public Long getVilleSurface() {
        return villeSurface;
    }

    public void setVilleSurface(Long villeSurface) {
        this.villeSurface = villeSurface;
    }

    public double getVilleLongitudeDeg() {
        return villeLongitudeDeg;
    }

    public void setVilleLongitudeDeg(double villeLongitudeDeg) {
        this.villeLongitudeDeg = villeLongitudeDeg;
    }

    public double getVilleLatitudeDeg() {
        return villeLatitudeDeg;
    }

    public void setVilleLatitudeDeg(double villeLatitudeDeg) {
        this.villeLatitudeDeg = villeLatitudeDeg;
    }

    public Date getVilleCreatedAt() {
        return villeCreatedAt;
    }

    public void setVilleCreatedAt(Date villeCreatedAt) {
        this.villeCreatedAt = villeCreatedAt;
    }

//    public Date getVilleCreatedAt(String villeCreatedAt){
//        try {
//            this.villeCreatedAt = (java.sql.Date) new SimpleDateFormat("yyyy-MM-dd").parse(villeCreatedAt);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        return villeCreatedAt;
//    }

}