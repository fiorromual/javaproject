package com.gendteam.project.model;

import org.springframework.beans.factory.annotation.Value;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "visited", schema = "public")
public class Visited implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id // Pout la base de données
    @GeneratedValue(strategy = GenerationType.AUTO) // Pout la base de données
    @Column(name = "id")  // Pout la base de données
    private Long id;

    @Column(name = "first_name", length = 75)
    private String firstName;

    @Column(name = "last_name", length = 75)
    private String lastName;

    @Column(name = "is_alive")
    @Value("true")
    private Boolean isAlive;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ephad.id")
    private Ephad ephadId;

    @Column(name = "info")
    private Long info;

    public Visited() {
    }

    public Visited(String firstName, String lastName) {
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Boolean getAlive() {
        return isAlive;
    }

    public void setAlive(Boolean alive) {
        isAlive = alive;
    }

    public Ephad getEphadId() {
        return ephadId;
    }

    public void setEphadId(Ephad ephadId) {
        this.ephadId = ephadId;
    }

    public Long getInfo() {
        return info;
    }

    public void setInfo(Long info) {
        this.info = info;
    }

    @Override
    public String toString() {
        return "Visited{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", isAlive=" + isAlive +
                ", ephadId=" + ephadId +
                ", info=" + info +
                '}';
    }

}

