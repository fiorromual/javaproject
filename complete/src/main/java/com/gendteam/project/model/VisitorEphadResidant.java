package com.gendteam.project.model;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "visitor_ephad_residant", schema = "springlabs")
public class VisitorEphadResidant  implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id // Pour la base de données
    @GeneratedValue(strategy = GenerationType.AUTO) // Pour la base de données
    @Column(name = "id") // Pour la base de données
    private Long id;


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user.id")
    private User userId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ephad.id")
    private Ephad ephadId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "visited.id")
    private Visited visitedId;

    @Column(name = "relation")
    private Integer relation;

    @Column(name = "comment", length = 400)
    private String comment;

    @Column(name = "is_active", columnDefinition = "integer default 1")
    private Integer isActive;

    public VisitorEphadResidant() {
    }

    public VisitorEphadResidant(User userId) {
        this.userId = userId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public Ephad getEphadId() {
        return ephadId;
    }

    public void setEphadId(Ephad ephadId) {
        this.ephadId = ephadId;
    }

    public Visited getVisitedId() {
        return visitedId;
    }

    public void setVisitedId(Visited visitedId) {
        this.visitedId = visitedId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getRelation() {
        return relation;
    }

    public void setRelation(Integer relation) {
        this.relation = relation;
    }

    public Integer getIsActive() {
        return isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }
}
