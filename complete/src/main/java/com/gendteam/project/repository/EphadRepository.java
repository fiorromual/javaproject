package com.gendteam.project.repository;

import com.gendteam.project.model.Ephad;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

;

@Repository
public interface EphadRepository extends PagingAndSortingRepository<Ephad, Long>  {

    @Query("SELECT COUNT(u) FROM Ephad u ")
    Long findCount();

    // Méthode FindAll
    Page<Ephad> findAll(Pageable pageable);

    Optional<Object> findById(long i);

    // Methode magique fonctionnelle pour le filtre
    Page<Ephad> findAllByVilleOrCodePostal(String ville, String codePostal, Pageable pageable);

    Ephad findOneById(Ephad ephadId);

    List<Ephad> findAll();


}
