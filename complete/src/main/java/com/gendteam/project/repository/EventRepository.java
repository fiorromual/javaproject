package com.gendteam.project.repository;

import com.gendteam.project.model.Ephad;
import com.gendteam.project.model.Event;
import com.gendteam.project.model.User;
import com.gendteam.project.model.Visited;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface EventRepository extends CrudRepository<Event, Long> {

	@Query(value = "from Event e where not(e.end < :from or e.start > :to) and e.visitedId = :visitedId")
	public List<Event> findBetween(@Param("from") @DateTimeFormat(iso = ISO.DATE_TIME) LocalDateTime start,
								   @Param("to") @DateTimeFormat(iso = ISO.DATE_TIME) LocalDateTime end,
								   @Param("visitedId") Visited visitedId);

//	@Query(value = "from Event e where not(e.end < :from or e.start > :to)")
//	public List<Event> findBetween(@Param("from") @DateTimeFormat(iso = ISO.DATE_TIME) LocalDateTime start,
//								   @Param("to") @DateTimeFormat(iso = ISO.DATE_TIME) LocalDateTime end);

	Optional<Object> findById(long id);

	Page<Event> findAllByIdOrVisitedId(Long id, Visited visitedId, Pageable pageable);

	Page<Event> findAll(Pageable pageable);

    Page<Event> findAllByUserId(User userId, Pageable pageable);

	Page<Event> findAllByUserIdAndStartAfter(Long userId, LocalDateTime tomorrow, Pageable pageable);

	Page<Event> findAllByEphadId(Ephad ephadId, Pageable pageable);



	List<Event> findAllByEphadIdAndStartAfterAndStartBeforeAndColor(Ephad ephadId, LocalDateTime myDay, LocalDateTime nextDay, String color);


	/**
	 *
	 * @param start
	 * @param ephadId
	 * @return
	 */
	@Query("SELECT COUNT(e) FROM Event e where e.start = : start  and e.ephadId = : ephad ")
	Integer findVisitsCount(@Param("event.getStart()") @DateTimeFormat(iso = ISO.DATE_TIME) LocalDateTime start,
				   @Param("visited.getEphadId()") Ephad ephadId);

	/**
	 *
	 * @param start
	 * @param ephadId
	 * @return
	 */
	int countAllByStartEqualsAndEphadId(LocalDateTime start, Ephad ephadId);

	/**
	 *
	 * @param start
	 * @param association
	 * @return
	 */
	int countByStartAndVisitedIdAndRelation(LocalDateTime start,Visited visitedId, Integer association);


	/**
	 *
	 * @param start date de debut de la visite
	 * @param visitedId instance du résidant
	 * @return retourne la valeur d'une visite à un résidant à une date-heure donnée
	 */
	int countByStartAndVisitedId(LocalDateTime start,Visited visitedId);

	/**
	 *
	 * @param start
	 * @param visited
	 * @return
	 */
	Event findByStartAndVisitedId(LocalDateTime start, Visited visited);

	Page<Event> findAllByStartBeforeAndAndUserId(LocalDateTime today,Long user, Pageable pageable);


	void deleteById(Long id);
}