package com.gendteam.project.repository;

import com.gendteam.project.model.FirstName;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FirstNameRepository extends PagingAndSortingRepository<FirstName, Long> {

    @Query("SELECT COUNT(u) FROM FirstName u ")
    Long findCount();

}