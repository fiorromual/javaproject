package com.gendteam.project.repository;

import com.gendteam.project.model.LastName;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LastNameRepository extends PagingAndSortingRepository<LastName, Long> {

    @Query("SELECT COUNT(u) FROM LastName u ")
    Long findCount();

}