package com.gendteam.project.repository;

import com.gendteam.project.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Integer>{

    Role findById(int i);

    Role findByName(String role_admin);

    Role findOneById(Long relation);
}
