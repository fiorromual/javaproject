package com.gendteam.project.repository;

import com.gendteam.project.model.Ephad;
import com.gendteam.project.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository  extends PagingAndSortingRepository<User, Long> {


    User findById(long i);

    User findByUsername(String username);

    User findByEphadId(long i);

//    Page<User> findAllByRolesIs( Long admin);

    @Query("SELECT u FROM User u WHERE u.username=:usernameOrEmail OR u.email=:usernameOrEmail")
	User findByUsernameOrEmail(@Param("usernameOrEmail") String usernameOrEmail);

    List<User> findAllByEphadIdAndRolesId_Id(Ephad ephadId, long i);

    List<User> findAllByRolesId_Id( long role_admin );

    void deleteById(Long id);
}
