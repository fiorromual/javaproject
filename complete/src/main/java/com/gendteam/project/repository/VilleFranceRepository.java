package com.gendteam.project.repository;

import com.gendteam.project.model.VilleFrance;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface VilleFranceRepository extends PagingAndSortingRepository<VilleFrance, Long> {

    Page<VilleFrance> findAll(Pageable pageable);

    List<VilleFrance> findAll();

    Optional<Object> findById(long i);

    Page<VilleFrance> findAllByVilleNom(String villeNom, Pageable pageable);

    // Methode magique fonctionnelle pour le filtre
    Page<VilleFrance> findAllByVilleNomOrVilleCodePostal(String villeNom, String villeCodePostal, Pageable pageable);


}
