package com.gendteam.project.repository;

import com.gendteam.project.model.Ephad;
import com.gendteam.project.model.Visited;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VisitedRepository extends PagingAndSortingRepository<Visited, Long> {
    

    Page<Visited> findAllByEphadId(Ephad ephad, Pageable pageable);

    Page<Visited> findAll(Pageable pageable);

    Visited findById(long i);

    Page<Visited> findAllByLastNameOrFirstName(String lastName,String firstName, Pageable pageable);

    List<Visited> findAllByEphadId(Ephad ephad);

    Iterable<Visited> findAll();

    Visited findByLastName(String visited);

    void deleteById(Long id);


    // Methode magique fonctionnelle pour le filtre



}
