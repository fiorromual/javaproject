package com.gendteam.project.repository;

import com.gendteam.project.model.Ephad;
import com.gendteam.project.model.User;
import com.gendteam.project.model.VisitorEphadResidant;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VisitorEphadResidantRepository  extends PagingAndSortingRepository<VisitorEphadResidant, Long> {

    Page<VisitorEphadResidant> findAll(Pageable pageable);

    Page<VisitorEphadResidant> findByUserId(User userId, Pageable pageable);

    Page<VisitorEphadResidant> findAllByEphadId(Ephad ephadId, Pageable pageable);

    VisitorEphadResidant findById(Long id);
}
