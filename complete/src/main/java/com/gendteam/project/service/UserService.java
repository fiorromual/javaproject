package com.gendteam.project.service;

import com.gendteam.project.model.User;

public interface UserService {

	void save(User user);

	void saveAdmin(User user);

	void saveMed(User user);

	User findByUsername(String username);

	void changeUserPassword(User user);


}
