package com.gendteam.project.service;

import com.gendteam.project.model.Role;
import com.gendteam.project.model.User;
import com.gendteam.project.repository.RoleRepository;
import com.gendteam.project.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
        public void save(User user) {
            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            // Integration des roles
            Role myRole = roleRepository.findOneById(user.getRelation());
            System.out.println("******************** relation :" + user.getRelation());
            Role[] arr = { myRole };
            user.setRoles(new HashSet<>(Arrays.asList(arr)));
//            user.setRoles(new HashSet<>(roleRepository.findAll()));
            userRepository.save(user);
    }

    @Override
    public void saveAdmin(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        Role myRole = roleRepository.findOneById((long) 1);
        Role[] arr = { myRole };
        user.setRoles(new HashSet<>(Arrays.asList(arr)));
        userRepository.save(user);
    }

    @Override
    public void saveMed(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        Role myRole = roleRepository.findOneById((long) 4);
        Role[] arr = { myRole };
        user.setRoles(new HashSet<>(Arrays.asList(arr)));
        userRepository.save(user);
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }


    @Override
    public void changeUserPassword(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userRepository.save(user);
    }

    public void standardSave(){

    }
}

