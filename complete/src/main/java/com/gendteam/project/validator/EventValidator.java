package com.gendteam.project.validator;


import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class EventValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return false;
    }

    @Override
    public void validate(Object o, Errors errors) {
    }

    public void validateDate(Object o, Errors errors) {

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "dateNews", "NotEmpty");
    }

}
