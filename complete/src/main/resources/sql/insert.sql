INSERT INTO `user` (`id`, `email`, `first_name`, `last_name`, `password`, `username`, `role_id`) VALUES
(1, 'admin@gmail.com', 'Admin', 'Admin', '$2a$10$bpNMKeaQXKpJ4JVxOHWvu.tZdmCLT9nKcZreJ/ELfCgmTCyhC7GPy', 'admin', 1),
(2, 'user@gmail.com', 'User', 'User', '$2a$10$TA.UfUqLa8uDeGkt95FfLeq7T5Y5vpDpzAtvJrHSLzLliY/PARXUq', 'user', 2);