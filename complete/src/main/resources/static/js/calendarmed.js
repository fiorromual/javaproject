dp.contextMenu = new DayPilot.Menu({
    items: [
        {
            text: "Demande famille",
            icon: "icon icon-blue",
            color: "#1066a8",
            onClick: function(args) { updateColor(args.source, args.item.color); }
        },
        {
            text: "Demande association",
            icon: "icon icon-green",
            color: "#6aa84f",
            onClick: function(args) { updateColor(args.source, args.item.color); }
        },
        {
            text: "Refus médecin",
            icon: "icon icon-yellow",
            color: "#f1c232",
            onClick: function(args) { updateColor(args.source, args.item.color); }
        },
        {
            text: "Accepté",
            icon: "icon icon-red",
            color: "#cc0000",
            onClick: function(args) { updateColor(args.source, args.item.color); }
        },

    ]
});