var nav = new DayPilot.Navigator("nav");
nav.showMonths = 1;
nav.skipMonths = 1;
nav.selectMode = "week";
nav.locale="fr-be";
nav.onTimeRangeSelected = function(args) {
    dp.startDate = args.day;
    dp.update();
    dp.events.load("api/events");
};
nav.init();


var dp = new DayPilot.Calendar("dp");
dp.viewType = "Week";
dp.locale = "fr-be";
dp.headerDateFormat = "dd/MM/yyyy";
dp.onTimeRangeSelected = function (args) {
    DayPilot.Modal.prompt("Créer une demande:", "Ma demande").then(function (modal) {
        var dp = args.control;
        dp.clearSelection();
        if (!modal.result) {
            return;
        }
        var params = {
            start: args.start.toString(),
            end: args.end.toString(),
            text: modal.result,
            resource: args.resource
        };
        $.ajax({
            type: 'POST',
            url: '../api/events/create',
            data: JSON.stringify(params),
            success: function (data) {
                dp.events.add(new DayPilot.Event(data));
                dp.message("Evènement créé");
            },
            contentType: "application/json",
            dataType: 'json'
        });
    });
};
// dp.onEventMove = function (args) {
//     var params = {
//         id: args.e.id(),
//         start: args.newStart.toString(),
//         end: args.newEnd.toString(),
//         resource: args.newResource
//     };
//     $.ajax({
//         type: 'POST',
//         url: '../api/events/move',
//         data: JSON.stringify(params),
//         success: function (data) {
//             dp.message("Evènement déplacé");
//         },
//         contentType: "application/json",
//         dataType: 'json'
//     });
// };
dp.onEventMove = function (args) {
    var params = {
        id: args.e.id()
    };
    $.ajax({
        type: 'POST',
        url: '../api/events/delete',
        data: JSON.stringify(params),
        success: function (data) {
            dp.message("Evènement suppriné");
        },
        contentType: "application/json",
        dataType: 'json'
    });
};
dp.onEventResize = function (args) {
    var params = {
        id: args.e.id(),
        start: args.newStart.toString(),
        end: args.newEnd.toString(),
        resource: args.e.resource()
    };
    $.ajax({
        type: 'POST',
        url: '../api/events/move',
        data: JSON.stringify(params),
        success: function (data) {
            dp.message("Evènement retouché");
        },
        contentType: "application/json",
        dataType: 'json'
    });
};
dp.onBeforeEventRender = function(args) {
    args.data.barColor = args.data.color;
    args.data.areas = [
        { top: 2, right: 2, icon: "icon-triangle-down", visibility: "Hover", action: "ContextMenu", style: "font-size: 12px; background-color: #f9f9f9; border: 1px solid #ccc; padding: 2px 2px 0px 2px; cursor:pointer;"}
    ];
};
dp.contextMenu = new DayPilot.Menu({
    items: [
        {
            text: "Demande famille",
            icon: "icon icon-blue",
            color: "#1066a8",
            onClick: function(args) { updateColor(args.source, args.item.color); }
        },
        {
            text: "Demande association",
            icon: "icon icon-green",
            color: "#6aa84f",
            onClick: function(args) { updateColor(args.source, args.item.color); }
        },
        {
            text: "Refus médecin",
            icon: "icon icon-yellow",
            color: "#f1c232",
            onClick: function(args) { updateColor(args.source, args.item.color); }
        },
        {
            text: "Accepté",
            icon: "icon icon-red",
            color: "#cc0000",
            onClick: function(args) { updateColor(args.source, args.item.color); }
        },

    ]
});
dp.init();

dp.events.load("../api/events");


function updateColor(e, color) {
    var params = {
        id: e.id(),
        color: color
    };
    $.ajax({
        type: 'POST',
        url: '../api/events/setColor',
        data: JSON.stringify(params),
        success: function (data) {
            e.data.color = color;
            dp.events.update(e);
            dp.message("Color updated");
        },
        contentType: "application/json",
        dataType: 'json'
    });
}
