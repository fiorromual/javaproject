

INSERT INTO `role` (`id`, `name`, `role_name`) VALUES
(1, 'ROLE_ADMIN', 'ROLE_ADMIN'),
(2, 'ROLE_USER', 'ROLE_USER'),
(3, 'ROLE_ASSO', 'ROLE_ASSO'),
(4, 'ROLE_MED', 'ROLE_MED'),
(5, 'ROLE_SUPADMIN', 'ROLE_SUPADMIN');
